# Learning Objectives

#### LO1. I can describe the role a switch plays in a computer network.
A switch serves as the central coordinator in a direct network connection, functioning like a router but at layer 2. It filters and forwards data using a switch table containing MAC addresses, interface details, and timestamps. Using store-and-forwarding, it avoids collisions and only transmits one frame at a time per interface. This eliminates the need for multiple access protocols and speeds up network delay. Additionally, switches enable throttling on different interfaces, making it easier to manage heavy traffic areas without affecting well-operating segments. Their 'plug-and-play' nature simplifies network setup and maintenance, benefiting large networks by reducing collisions, efficiently forwarding data, and optimizing connections.

Switches are unique in that they can perform a lot of tasks, making them great tools in a computer network. Switches regulate traffic, provide a MAC address table, forward packets, and allow for data to be transmitted efficiently.

**Traffic Control:** A switch receives data packets from devices connected to its ports. It examines the destination MAC address of each incoming packet and determines the appropriate port to forward the packet to, based on its MAC address table.

**MAC Address Table:** The switch builds and maintains a table (MAC address table or CAM table) that maps MAC addresses to the corresponding physical ports. This table is populated as devices send data, enabling the switch to efficiently direct traffic to the correct destination without broadcasting to all ports.

**Packet Forwarding:** When a packet arrives at the switch, it checks its MAC address table to find the port associated with the destination MAC address. If the destination is known, the switch forwards the packet only to the port where the destination device is connected, reducing unnecessary traffic on the network.

**Efficient Data Transmission:** By using the MAC address table to direct traffic, switches enable more efficient data transmission, reducing collisions and unnecessary data propagation.

#### LO2. I can explain the problem ARP solves, how it solves the problem, and can simulate an ARP table given a simple LAN scenario.
ARP (Address Resolution Protocol) solves the problem of matching IP addresses used for network communication with MAC addresses used for local data delivery within a network. It dynamically maps IP addresses to MAC addresses, allowing devices to find and communicate with each other effectively on the same network.

ARP resolves the issue by matching IP addresses to MAC addresses within a local network through dynamic discovery and mapping. It follows a sequence which includes broadcasting requests, response and mapping, and dynamic updating. By facilitating this communication and dynamic mapping process, ARP enables devices to find each other on the network and correctly direct data to the intended recipients by matching the IP and MAC addresses for local delivery.

**Broadcasting Requests:** When a device wants to communicate with another device using an IP address but lacks the corresponding MAC address, it sends out a broadcast ARP request across the network, asking which device owns that IP address and requesting its MAC address.

**Response and Mapping:** The device that holds the requested IP address responds to the broadcast with its MAC address. This response allows the requesting device to create or update its ARP cache, forming a temporary mapping between the IP and MAC addresses.

**Dynamic Updating:** As devices communicate and ARP requests and responses occur, ARP caches are dynamically updated, ensuring that devices maintain accurate mappings of IP addresses to MAC addresses within the local network.

Suppose there exists a simple Local Area Network that has 3 devices. Let us assume that each device has its own IP and MAC address. At the beginning, nothing exists in the ARP table since there have been no communications among the devices.

| ARP Table for Device A |
IP Address  ----- empty
MAC Address ----- empty

| ARP Table for Device B |
IP Address  ----- empty
MAC Address ----- empty

| ARP Table for Device C |
IP Address  ----- empty
MAC Address ----- empty

Now suppose Device B needs something from Device A, Device B will send an ARP request on the network, asking for the MAC address associated with the IP address of Device A. After that, Device A will recognize its IP address in the ARP request and will reply with its MAC address. The ARP table will then be updated.

#### LO3. I can explain CSMA/CS, how it differs from CSMA/CD, what problems it addresses, and how it solves them.
CSMA (Carrier Sense Multiple Access) is a protocol used in network environments to manage how devices on a shared communication channel access and transmit data. CSMA/CS (Collision Sense Multiple Access) and CSMA/CD (Collision Detection) are variations of this protocol with distinct methods of handling collisions, which occur when two or more devices attempt to transmit data simultaneously on the same channel, causing interference.

**CSMA/CS (Collision Sense Multiple Access):**
**Collision Detection Mechanism:** CSMA/CS relies on detecting the occurrence of collisions before they happen by listening to the carrier or channel for any ongoing transmission before sending data. If the channel is idle, the device starts transmitting its data. If it detects another transmission, it defers its own transmission, waiting for a period before reattempting.

**CSMA/CD (Collision Detection):**
**Collision Detection and Response:** CSMA/CD takes the collision avoidance of CSMA/CS a step further by actively listening to the network while transmitting data. If a collision is detected (when the device senses its own transmitted signal is interfered with), it stops transmitting immediately. After detecting a collision, CSMA/CD initiates a backoff algorithm, causing the device to wait for a random period before retransmitting.
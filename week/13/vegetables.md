# Kurose, Chapter 6 & 7 Questions

#### R10. Suppose nodes A, B, and C each attach to the same broadcast LAN (through their adapters). If A sends thousands of IP datagrams to B with each encapsulating frame addressed to the MAC address of B, will C’s adapter process these frames? If so, will C’s adapter pass the IP datagrams in these frames to the network layer C? How would your answers change if A sends frames with the MAC broadcast address?
C's adapter will handle the frames received but won't forward the datagrams up the protocol stack. However, if the LAN broadcast address is utilized, C's adapter will both handle the frames and forward the datagrams up the protocol stack.

#### R11. Why is an ARP query sent within a broadcast frame? Why is an ARP response sent within a frame with a specific destination MAC address?
An ARP query is broadcasted because the querying host lacks information about which adapter address matches the IP address in question. Conversely, for the response, the sending node possesses the adapter address to which the response is directed, eliminating the necessity for a broadcast frame. This avoids the need for all other LAN nodes to process the broadcast frame.

#### P14. Consider three LANs interconnected by two routers, as shown in Figure 6.33.
##### a. Assign IP addresses to all of the interfaces. For Subnet 1 use addresses of the form 192.168.1.xxx; for Subnet 2 uses addresses of the form 192.168.2.xxx; and for Subnet 3 use addresses of the form 192.168.3.xxx.
To assign IP addresses to the interfaces in the three LANs, following the given format:

Subnet 1:
Router 1 Interface 0: 192.168.1.1
Host A: 192.168.1.2
Host B: 192.168.1.3

Subnet 2:
Router 1 Interface 1: 192.168.2.1
Router 2 Interface 0: 192.168.2.2
Host C: 192.168.2.3
Host D: 192.168.2.4

Subnet 3:
Router 2 Interface 1: 192.168.3.1
Host E: 192.168.3.2
Host F: 192.168.3.3

##### b. Assign MAC addresses to all of the adapters.
Assigning MAC addresses to the adapters:

Router 1 Interface 0: MAC_A
Router 1 Interface 1: MAC_B
Router 2 Interface 0: MAC_C
Router 2 Interface 1: MAC_D
Host A: MAC_E
Host B: MAC_F
Host C: MAC_G
Host D: MAC_H
Host E: MAC_I
Host F: MAC_J

##### c. Consider sending an IP datagram from Host E to Host B. Suppose all of the ARP tables are up to date. Enumerate all the steps, as done for the single-router example in Section 6.4.1.
Steps for sending an IP datagram from Host E to Host B:

Host E consults its ARP table to locate the MAC address for 192.168.3.1 (Router 2 Interface 1), assuming the MAC address is MAC_D. Then, it packages the IP datagram intended for Host B within an Ethernet frame.

Source IP: 192.168.3.2 (Host E's IP address)
Destination IP: 192.168.1.3 (Host B's IP address)
Source MAC: MAC_I (Host E's MAC address)
Destination MAC: MAC_D (Router 2 Interface 1's MAC address)

Subsequently, Host E transmits the Ethernet frame across its LAN. The frame arrives at Router 2 Interface 1.

Router 2, upon receiving the Ethernet frame, examines its ARP table to find the MAC address for 192.168.1.3 (Host B), assuming the MAC address is MAC_F. It then encapsulates the received Ethernet frame within a new Ethernet frame.

Source IP: 192.168.3.1 (Router 2 Interface 1's IP address)
Destination IP: 192.168.1.3 (Host B's IP address)
Source MAC: MAC_D (Router 2 Interface 1's MAC address)
Destination MAC: MAC_F (Host B's MAC address)

Router 2 forwards the new Ethernet frame across its LAN. Upon arrival, Host B receives the frame, extracts the IP datagram, and processes it accordingly.

##### d. Repeat (c), now assuming that the ARP table in the sending host is empty(and the other tables are up to date)
Assuming the ARP table in the sending host (Host E) is empty:

Host E examines its empty ARP table, realizing the absence of the MAC address for 192.168.3.1 (Router 2 Interface 1). Consequently, Host E initiates an ARP broadcast message:

Source IP: 192.168.3.2 (Host E's IP address)
Source MAC: MAC_E (Host E's MAC address)
Target IP: 192.168.3.1 (Router 2 Interface 1's IP address)

The ARP broadcast reaches Router 2 Interface 1, allowing it to learn Host E's MAC address (MAC_E) from the received ARP message. In response, Router 2 Interface 1 sends an ARP unicast reply back to Host E:

Source IP: 192.168.3.1 (Router 2 Interface 1's IP address)
Source MAC: MAC_D (Router 2 Interface 1's MAC address)
Target IP: 192.168.3.2 (Host E's IP address)
Target MAC: MAC_E (Host E's MAC address)

Upon reception of the ARP reply, Host E updates its ARP table, acquiring the MAC address of Router 2 Interface 1. Subsequently, Host E encapsulates the IP datagram destined for Host B within an Ethernet frame.

Source IP: 192.168.3.2 (Host E's IP address)
Destination IP: 192.168.1.3 (Host B's IP address)
Source MAC: MAC_E (Host E's MAC address)
Destination MAC: MAC_D (Router 2 Interface 1's MAC address)

Host E transmits the Ethernet frame across its LAN, following the same steps as in part c from Step 4 onwards to reach Host B.

#### P15. Consider Figure 6.33. Now we replace the router between subnets 1 and 2 with a switch S1, and label the router between subnets 2 and 3 as R1.
##### a. Consider sending an IP datagram from Host E to Host F. Will Host E ask router R1 to help forward the datagram? Why? In the Ethernet frame containing the IP datagram, what are the source and destination IP and MAC addresses?
Host E can verify the subnet prefix of Host F's IP address, realizing that F is within the same LAN. Consequently, E won't forward the packet to the default router, R1. The Ethernet frame from E to F would have these details:

Source IP: E’s IP address
Destination IP: F’s IP address
Source MAC: E’s MAC address
Destination MAC: F’s MAC address

##### b. Suppose E would like to send an IP datagram to B, and assume that E’s ARP cache does not contain B’s MAC address. Will E perform an ARP query to find B’s MAC address? Why? In the Ethernet frame (containing the IP datagram destined to B) that is delivered to router R1, what are the source and destination IP and MAC addresses?
No, as they are not within the same LAN. E can ascertain this by examining B’s IP address. In the Ethernet frame from E to R1, the details would be:

Source IP: E’s IP address
Destination IP: B’s IP address
Source MAC: E’s MAC address
Destination MAC: The MAC address of R1’s interface connecting to Subnet 3.

##### c. Suppose Host A would like to send an IP datagram to Host B, and neither A’s ARP cache contains B’s MAC address nor does B’s ARP cache contain A’s MAC address. Further suppose that the switch S1’s forwarding table contains entries for Host B and router R1 only. Thus, A will broadcast an ARP request message. What actions will switch S1 perform once it receives the ARP request message? Will router R1 also receive this ARP request message? If Figure 6.33 ♦ Three subnets, interconnected by routers so, will R1 forward the message to Subnet 3? Once Host B receives this ARP request message, it will send back to Host A an ARP response message. But will it send an ARP query message to ask for A’s MAC address? Why? What will switch S1 do once it receives an ARP response message from Host B?
Switch S1 will broadcast the Ethernet frame on the interface to subnet 2 (due to receiving the frame on its interface to subnet 1) because the destination address in the received ARP frame is a broadcast address. Consequently, it deduces that Host A resides on Subnet 1, connected to S1 at the interface linking to Subnet 1. S1 updates its forwarding table to include an entry for Host A.

While router R1 receives this ARP request message, it won’t proceed to forward the message to Subnet 3.

As for Host B, it won’t send an ARP query message to obtain A’s MAC address since this address can be acquired from A’s query message.

Upon receiving B’s response message, switch S1 refrains from adding an entry for Host B to its forwarding table (as explicitly mentioned in the question). It then discards the received frame because the destination host A is on the same interface as host B, indicating that A and B are on the same LAN segment.

#### R3. What are the differences between the following types of wireless channel impairments: path loss, multipath propagation, interference from other sources?
Path loss entails a reduction in signal strength, resulting in weaker signals received by the receiver. Multipath propagation involves various signal paths of varying lengths, dependent on multiple signals. Interfaces from various sources facilitate the transmission of signals at the same frequency within wireless network systems.

#### R4. As a mobile node gets farther and farther away from a base station, what are two actions that a base station could take to ensure that the loss probability of a transmitted frame does not increase?
The two possible ways are, 1) increasing the transmission power, and 2) reducing the transmission rate.

#### P6. In step 4 of the CSMA/CA protocol, a station that successfully transmits aframe begins the CSMA/CA protocol for a second frame at step 2, rather than at step 1. What rationale might the designers of CSMA/CA have had in mind by having such a station not transmit the second frame immediately (if the channel is sensed idle)?
Imagine a scenario where wireless station H1 intends to transmit 1000 long frames, perhaps as an Access Point (AP) sending an MP3 file to another wireless station. Initially, H1 is the sole station looking to transmit. However, halfway through sending its first frame, H2 also wants to transmit a frame.

Assuming every station can detect signals from all other stations (no hidden terminals), H2 senses the channel as busy before attempting transmission. Consequently, H2 adopts a random backoff strategy, waiting for an idle channel.

Now, if after sending its first frame, H1 restarts the process (waits a brief period—DIFS—then starts transmitting the second frame), H1's second frame will be transmitted while H2 remains in backoff, awaiting an idle channel. Consequently, H1 should manage to transmit all 1000 frames before H2 accesses the channel.

Alternatively, if H1, after sending a frame, proceeds to step 2 (selects a random backoff value), it offers a fair opportunity for H2 to access the channel. This approach was designed with fairness in mind, ensuring an equitable chance for both stations to access the channel.

#### P7. Suppose an 802.11b station is configured to always reserve the channel with the RTS/CTS sequence. Suppose this station suddenly wants to transmit 1,500 bytes of data, and all other stations are idle at this time. As a function of SIFS and DIFS, and ignoring propagation delay and assuming no bit errors, calculate the time required to transmit the frame and receive the acknowledgement.
A frame devoid of data spans 32 bytes in length. Given a transmission rate of 11 Mbps, transmitting a control frame (e.g., RTS, CTS, or ACK) takes approximately 23 microseconds (calculated as 256 bits divided by 11 Mbps).

On the other hand, the transmission duration for a data frame totals approximately 751 microseconds (derived from 8256 bits divided by 11 Mbps).
# VintMania!

### Next week (21 Nov) we will be attending a Vint Cerf lecture during lunch. So we're better able to appreciate the lecture, please answer the following questions.

#### 1. Vint Cerf is known as "one of the fathers of the Internet". Why is he known as this?
Vint Cerf is often referred to as one of the "fathers of the internet" due to his significant contributions to the development of the TCP/IP protocols, which form the foundation of the internet. These protocols are fundamental to how data is transmitted and routed across interconnected networks, allowing different systems to communicate with each other regardless of their underlying hardware or architecture. Cerf's work was crucial in creating the technical architecture and standards that enabled the internet to grow and expand into the global network we use today. His pioneering efforts in shaping the internet's fundamental protocols have earned him recognition as a key figure in its development.

#### 2. Find 3 surprising Vint Cerf facts.
1. Vint Cerf is involved in creating an "Interplanetary Internet" protocol. As a computer scientist at NASA's Jet Propulsion Laboratory, he's been working on developing communication protocols that could be used for long-distance space missions.
2. Despite being a tech pioneer, Cerf is known for using a yellow legal pad and pen for note-taking and drafting ideas rather than relying solely on digital tools.
3. Cerf holds numerous prestigious titles and awards, including being a recipient of the Presidential Medal of Freedom, the National Medal of Technology, and the Turing Award, often referred to as the Nobel Prize of computing.

#### 3. Develop 2 interesting and insightful questions you'd like to ask him. For each question, describe why it is interesting and insightful.
1. What do you think lead you most to developing a communication system that would be so widely used today and did you think it would become this widespread as it is today?
- I always picture myself inventing something in the future, but I have no idea how I will get there.
2. Were you surprised there were no other large communication protocols at the time of you creating the TCP/IP model?
- Sometimes I think of great ideas, but only soon after to find out that they were already explored.
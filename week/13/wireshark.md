# Wireshark Lab: Ethernet and ARP v8.0

## Introduction
The purpose of this lab is to investigate the Ethernet protocol and ARP protocol. The ARP protocol is used by an IP device and used to determine the IP address of a remote interface whose Ethernet address is known.

## Process
Following the process listed on the course website, there were a few obstacles that I was forced to overcome. I am unable to open ip verison 8.1 on both my lab machine and personal laptop, so I can only use v8.0. Additionally, I had to follow the trace packet since I know my laptop does not work well with wireshark (no traffic is captured), and since we are unable to clear our history on our personal computers.

## 1. Capturing and analyzing Ethernet frames
#### 1. What is the 48-bit Ethernet address of your computer?
The Ethernet address of my computer is `00:80:ad:73:8d:ce`.

#### 2. What is the 48-bit destination address in the Ethernet frame? Is this the Ethernet address of gaia.cs.umass.edu? (Hint: the answer is no). What device has this as its Ethernet address? [Note: this is an important question, and one that students sometimes get wrong. Re-read pages 468-469 in the text and make sure you understand the answer here.]
The destination address in the Ethernet frame is `00:06:25:da:af:73`. This is not the address of gaia.cs.umass.edu, but that of my router. The reason is because that is the link used to get off of the subnet.

#### 3. Give the hexadecimal value for the two-byte Frame type field. What upper layer protocol does this correspond to?
The hex value for the two-byte Frame type field is 0x0800, which is the IP protocol.

#### 4. How many bytes from the very start of the Ethernet frame does the ASCII “G” in "GET” appear in the Ethernet frame?
The "G" is "GET" appears to be 52 bytes from the very start of the Ethernet frame.

#### 5. What is the value of the Ethernet source address? Is this the address of your computer, or of gaia.cs.umass.edu (Hint: the answer is no). What device has this as its Ethernet address?
The value is `00:d0:59:a9:3d:68` and is neither the address of my computer or gaia.cs.umass.edu. It is that of the router because that is the link used to get off of the subnet.

#### 6. What is the destination address in the Ethernet frame? Is this the Ethernet address of your computer? 
The destination address `00:80:ad:73:8d:ce` is the address of my computer.

#### 7. Give the hexadecimal value for the two-byte Frame type field. What upper layer protocol does this correspond to?
The hex value for the Frame type field is 0x0800. This value is the IP protocol.

#### 8. How many bytes from the very start of the Ethernet frame does the ASCII “O” in “OK” (i.e., the HTTP response code) appear in the Ethernet frame?
The ASCII "O" in "OK" also appears to be 52 bytes from the very start of the Ethernet frame.

## 2. The Address Resolution Protocol
#### 9. Write down the contents of your computer’s ARP cache. What is the meaning of each column value?
Internet Address contains IP, Physical Address contains MAC address, and type indicates protocol type.

#### 10. What are the hexadecimal values for the source and destination addresses in the Ethernet frame containing the ARP request message?
The hex value for the source address is `00:d0:59:a9:3d:68`. The hex value for the destination address is `ff:ff:ff:ff:ff:ff`, which stands for the broadcast address.

#### 11. Give the hexadecimal value for the two-byte Ethernet Frame type field. What upper layer protocol does this correspond to?
The hex value for the Ethernet Frame type field is 0x0806, and it stands for ARP.

#### 12. Download the ARP specification from ftp://ftp.rfc-editor.org/in-notes/std/std37.txt. A readable, detailed discussion of ARP is also at http://www.erg.abdn.ac.uk/users/gorry/course/inet-pages/arp.html.
##### a. How many bytes from the very beginning of the Ethernet frame does the ARP opcode field begin?
The ARP opcode field begins 20 bytes from the very beginning of the Ethernet frame.

##### b. What is the value of the opcode field within the ARP-payload part of the Ethernet frame in which an ARP request is made?
The value of the opcode field within the ARP-payload part of the Ethernet frame in which an ARP request is made is 0x0001, which stands for request.

##### c. Does the ARP message contain the IP address of the sender?
Yes, the ARP message contains the IP address of the sender.

##### d. Where in the ARP request does the “question” appear – the Ethernet address of the machine whose corresponding IP address is being queried?
The question appears in the "Target MAC address" field.

#### 13. Now find the ARP reply that was sent in response to the ARP request.
##### a. How many bytes from the very beginning of the Ethernet frame does the ARP opcode field begin?
The ARP opcode field begins 20 bytes from the very beginning of the Ethernet frame.

##### b. What is the value of the opcode field within the ARP-payload part of the Ethernet frame in which an ARP response is made?
The hex value for the opcode field within the ARP-payload part of the Ethernet frame is 0x0002, which stands for reply.

##### c. Where in the ARP message does the “answer” to the earlier ARP request appear – the IP address of the machine having the Ethernet address whose corresponding IP address is being queried?
The answers appears in the "Sender MAC address" field.

#### 14. What are the hexadecimal values for the source and destination addresses in the Ethernet frame containing the ARP reply message?
The hexadecimal values for the source and destination addresses in the Ethernet frame containing the ARP reply message is `00:80:ad:73:8d:ce` and `ff:ff:ff:ff:ff:ff`.

#### 15. Open the ethernet-ethereal-trace-1 trace file in http://gaia.cs.umass.edu/wireshark-labs/wireshark-traces.zip. The first and second ARP packets in this trace correspond to an ARP request sent by the computer running Wireshark, and the ARP reply sent to the computer running Wireshark by the computer with the ARP-requested Ethernet address. But there is yet another computer on this network, as indicated by packet 6 – another ARP request. Why is there no ARP reply (sent in response to the ARP request in packet 6) in the packet trace?
There is no reply in the trace because it is only found at the machine who sent the request. The ARP request is broadcast, while the reply is sent back to the sender's Ethernet address.
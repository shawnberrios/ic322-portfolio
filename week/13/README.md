# Week 13: More Link Layer

This week talks more about the link layer!

* [Wireshark](week/13/wireshark.md)
* [Partner Feedback](https://gitlab.usna.edu/atrayn1/ic322-portfolio/-/issues/23)
* [I ate my vegetables](week/13/vegetables.md)
* [Learning Objectives](week/13/learning-objectives.md)
* [VintMania](week/13/vintmania.md)
# Week 2: The Application Layer: HTTP

This week introduces the first layer of the Five-layer Internet protocol stack and how HTTP is involved.

* [Main](wireshark.md)
* [I ate my vegetables](week/2/vegetables.md)
* [Learning objectives](week/2/learning-objectives.md)
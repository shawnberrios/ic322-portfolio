# Wireshark HTTP Lab: HTTP v8.0

## Introduction

The purpose of this lab to expand on the fundamental concepts of Wireshark that was learned on the last lab. This lab is meant to investigate specific protocols in operation by exploring the basic GET/response interaction, HTTP message formats, retrieving large HTML files, retrieving HTML files with embedded objects, and HTTP authentication and security.

## Process

I simply followed the instructions entirely from the listed URL address, [click me!](http://www-net.cs.umass.edu/wireshark-labs/Wireshark_HTTP_v8.0.pdf) (version 8.0).

I was unable to receive any HTML packets during part 1 of the lab, so I downloaded a zip trace file that was provided. This trace file is used if you're unable to run Wireshark on a live connection or if you're having any trouble with your network. I am using an outdated version of the lab since version 8.1 didn't work on the website, which could be the reason I was unable to follow with my own browser.

## 1. The Basic HTTP GET/response interaction

#### 1. Is your browser running HTTP version 1.0 or 1.1? What version of HTTP is the server running?

Both the browser and server are running HTTP version 1.1. I know this because in both the request and response messages, `HTTP/1.1` is displayed.

![quest1](week/2/quest1.PNG)

#### 2. What languages (if any) does your browser indicate that it can accept to the server?

The only language that my browser indicates that can be accepted to the server is English. I know this because under 'Hypertext Transfer Protocol,' it says `Accept-Language: en-us, en;q=0.50\r\n`.

#### 3. What is the IP address of your computer? Of the gaia.cs.umass.edu server?

My IP address is `192.168.1.102` and the IP address of the server is `128.119.245.12`. This is indicated under source for both the request and response GET message. You can also track the request and see that its destination is gaia.cs.umass.edu's IP.

![quest2](week/2/quest2.PNG)

#### 4. What is the status code returned from the server to your browser?

The status code that was returned from the server to my browser was `200 OK`. This status code indicates a success status and that the request has succeeded [Mozilla Developer](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/200).

#### 5. When was the HTML file that you are retrieving last modified at the server?

This HTML file was last modified at the server on `Last-Modified: Tue, 23 Sep 2003 05:29:00 GMT\r\n`. This can be found by navigating to the response GET message and going to the 'Hypertext Transfer Protocol' dropdown.

#### 6. How many bytes of content are being returned to your browser?

There are 73 bytes that are being returned to my browser.

#### 7. By inspecting the raw data in the packet content window, do you see any headers within the data that are not displayed in the packet-listing window? If so, name one.

When manipulating multiple windows, I was able to find that the file's header consisted of 20 bytes. It was followed with '(5)'. When looking at the raw data, the header was linked to a value of `50` and `p`.

## 2. The HTTP CONDITIONAL GET/response interaction

#### 8. Inspect the contents of the first HTTP GET request from your browser to the server. Do you see an "IF-MODIFIED-SINCE" line in the HTTP GET?

No.

#### 9. Inspect the contents of the server response. Did the server explicitly return the contents of the file? How can you tell?

Yes. It was very easy for the server to explicitly return the contents of the file, especially since it was written in HTML format. If you navigate to 'Line-based text data: text/html (10 lines),' you will see:

![quest9](week/2/quest9.PNG)

#### 10. Now inspect the contents of the second HTTP GET request from your browser to the server? Do you see an "IF-MODIFIED-SINCE:" line in the HTTP GET? If so, what information follows the "IF-MODIFIED-SINCE:" header?

Wow, there is an "IF-MODIFIED-SINCE" line. I stand corrected.

![quest10](week/2/quest10.PNG)

#### 11. What is the HTTP status code and phrase returned from the server in reponse to this second HTTP GET? Did the server explicitly return the contents of the file?

The HTTP status code of the second HTTP get request is `304 Not Modified`. Since this was the case, the server did not return the contents of the file to the browser. The reason for this is because servers have a way of not sending additional data, especially if the user has the most up-to-date version. More specifically, when a server sends the HTTP 304 response code. It indicates that the web page has not changed since the last time you accessed it (in this case, when we refreshed the page or re-entered the URL). So instead of sending you old data that you already have, your browser retrieves the cached version of the web page that is located in your local storage.

## 3. Retrieving Long Documents

#### 12. How many HTTP GET request messages did your browser send? Which packet number in the trace contains the GET message for the Bill of Rights?
The browser sent *one* HTTP GET request. The packet number in the trace that contains the GET message for the Bill of Rights is 8.

![quest12](week/2/quest12.PNG)

#### 13. Which packet number in the trace contains the status code and phrase associated with the response to the HTTP GET request?

The packet number that corresponds to the HTTP GET request and contains the status code is 14.

![quest13](week/2/quest13.PNG)

#### 14. What is the status code and phrase in the response?

The status code and phrase is `200 OK` indicating success.

#### 15. How many data-containing TCP segments were needed to carry the single HTTP response and text of the Bill of Rights?

A total of five TCP segments were needed to carry the single HTTP response and text of the Bill of Rights, since the HTML file is too large to fit into one TCP packet.

![quest15](week/2/quest15.PNG)

## 4. HTML Documents with Embedded Objects

#### 16. How many HTTP GET request messages did your browser send? To which Internet addresses were these GET requests sent?
My browser sent a total of 3 HTTP GET request messages. They were sent to `128.119.245.12`, `165.193.123.218`, and `134.231.6.82`. I know this because each request was sent from the same source of `192.168.1.102`.

#### 17. Can you tell whether your browser downloaded the two images serially, or whether they were downloaded from the two web sites in parallel. Explain?
My browser downloaded the images in parallel. The reason I know this is because the second GET request did not wait for the first GET request to receive its reponse. When going through the packet list, the list went:

1. Initial GET Request
2. GET Response connecting browser and server
3. First image GET request
4. Second image GET request
5. First image GET response
6. Second image GET response

## 5. HTTP Authentication

#### 18. What is the server's response (status code and phrase) in response to the initial HTTP GET message from your browser?

The server's response to the initial HTTP GET message from the browser was `401 Authentication Required`, indicating that more needs to be done from the client side because the server lacks valid authentiation credentials for the requested resource [Mozilla Dev](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/401).

#### 19. When your browser's sends the HTTP GET message for the second time, what new field is included in the HTTP GET message?

The new field that is located in the second HTTP GET message is `Authorization`.

Frame 65: 622 bytes on wire (4976 bits), 622 bytes captured (4976 bits)\
Ethernet II, Src: Dell_4f:36:23 (00:08:74:4f:36:23), Dst: LinksysG_da:af:73 (00:06:25:da:af:73)\
Internet Protocol Version 4, Src: 192.168.1.102, Dst: 128.119.245.12\
Transmission Control Protocol, Src Port: 4342, Dst Port: 80, Seq: 1, Ack: 1, Len: 568\
Hypertext Transfer Protocol\
    GET /ethereal-labs/protected_pages/lab2-5.html HTTP/1.1\r\n\
        [Expert Info (Chat/Sequence): GET /ethereal-labs/protected_pages/lab2-5.html HTTP/1.1\r\n]\
            [GET /ethereal-labs/protected_pages/lab2-5.html HTTP/1.1\r\n]\
            [Severity level: Chat]\
            [Group: Sequence]\
        Request Method: GET\
        Request URI: /ethereal-labs/protected_pages/lab2-5.html\
        Request Version: HTTP/1.1\
    Host: gaia.cs.umass.edu\r\n\
    User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.0.2) Gecko/20021120 Netscape/7.01\r\n\
    Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,video/x-mng,image/png,image/jpeg,image/gif;q=0.2,text/css,*/*;q=0.1\r\n\
    Accept-Language: en-us, en;q=0.50\r\n\
    Accept-Encoding: gzip, deflate, compress;q=0.9\r\n\
    Accept-Charset: ISO-8859-1, utf-8;q=0.66, *;q=0.66\r\n\
    Keep-Alive: 300\r\n\
    Connection: keep-alive\r\n\
    **Authorization: Basic ZXRoLXN0dWRlbnRzOm5ldHdvcmtz\r\n**\
    \r\n\
    [Full request URI: http://gaia.cs.umass.edu/ethereal-labs/protected_pages/lab2-5.html]\
    [HTTP request 1/1]\
    [Response in frame: 68]\

# Kurose, Chapter 4 Questions

#### R23. Visit a host that uses DHCP to obtain its IP address, network mask, default router, and IP address of its local DNS server. List these values.
- IPv4 address: 10.25.134.191
- Network Mask: 255.255.255.0
- Default Router: 10.25.128.1
- DNS IP Address: 10.1.74.10

#### R29. What is a private network address? Should a datagram with a private network address ever be present in the larger public Internet? Explain.
A private network typically operates within the IP address space of 10.0.0.0/24. More specifically, private IP addresses are classified according to their block sizes. I have attached an image for reference below. The broader 10.0.0.0/8 range is specifically designated for the exclusive use of private networks. This allocation results in a scenario where numerous internet-connected devices share this identical block of IP addresses. As a consequence, IP addresses within this range exclusively hold significance within their respective private networks, necessitating the utilization of a NAT-assigned IP address and port for global internet access. If these IP addresses were accessible on the broader public internet, it would lead to a proliferation of identical IP addresses, hindering the accurate delivery of datagrams to their intended destinations.

![PrivateIPs](week/8/private-ips.PNG)

#### R26. Suppose you purchase a wireless router and connect it to your cable modem. Also suppose that your ISP dynamically assigns your connected device (that is, your wireless router) one IP address. Also suppose that you have five PCs at home that use 802.11 to wirelessly connect to your wireless router. How are IP addresses assigned to the five PCs? Does the wireless router use NAT? Why or why not?
My five PCs will receive IP addresses through NAT, facilitated by the router. The reason for this lies in the fact that my Internet Service Provider (ISP) has provided only one IP address for my router. Consequently, NAT serves one method by granting IP addresses to my PCs, enabling them to connect to the internet. This will be accomplished by assigning each PC an IP address within the 10.0.0.0/24 subnet. Subsequently, when the PCs transmit datagrams to the internet, the router will pair their IP addresses with random ports and retain this information in the NAT translation table.

#### P15. Consider the topology shown in Figure 4.20. Denote the three subnets with hosts (starting clockwise at 12:00) as Networks A, B, and C. Denote the subnets without hosts as Networks D, E, and F.
##### a. Assign network addresses to each of these six subnets, with the following constraints: All addresses must be allocated from 214.97.254/23; Subnet A should have enough addresses to support 250 interfaces; Subnet B should have enough addresses to support 120 interfaces; and Subnet C should have enough addresses to support 120 interfaces. Of course, subnets D, E and F should each be able to support two interfaces. For each subnet, the assignment should take the form a.b.c.d/x or a.b.c.d/x – e.f.g.h/y. b. Using your answer to part (a), provide the forwarding tables (using longest prefix matching) for each of the three routers.
Subnet A: 214.97.255 / 24
8 bits=256 addresses
Subnet B: 214.97.254.0 / 25 – 214.97.254.0 / 29
7 bits-3 bits=120 addresses
Subnet C: 214.97.254.128 / 25
7 bits=128 addresses
Subnet D: 214.97.254.0 / 31
1 bit=2 addresses
Subnet E: 214.97.254.2 / 31
1 bit=2 addresses
Subnet F: 214.97.254.4 / 30
2 bits=4 addresses

##### b. Using your answer to part (a), provide the forwarding tables (using longest prefix matching) for each of the three routers.
Router 1
Prefix                                      Interface
11010110 01100001 11111111                  0
11010110 01100001 11111110 0000000          3
11010110 01100001 11111110 000001           5

Router 2
Prefix                                      Interface
11010110 01100001 11111111 000001           5
11010110 01100001 11111110 0000001          4
11010110 01100001 11111110 1                2

Router 3
Prefix                                      Interface
11010110 01100001 11111111 0000000          3
11010110 01100001 11111110 0                1
11010110 01100001 11111110 0000001          4

#### P16. Use the whois service at the American Registry for Internet Numbers(http://www.arin.net/whois) to determine the IP address blocks for threeuniversities. Can the whois services be used to determine with certainty the geographical location of a specific IP address? Use www.maxmind.com to determine the locations of the Web servers at each of these universities.
University of Missouri-151.101.2.216
Washington University in St. Louis-128.252.160.5
Florida State University-146.201.111.62

#### P18. Consider the network setup in Figure 4.25. Suppose that the ISP instead assigns the router the address 24.34.101.225 and that the network address of the home network is 192.168.0/24.
##### a. Assign addresses to all interfaces in the home network.
**Home address:**
192.168.1.1
192.168.1.2
192.168.1.3
192.168.1.4
  
##### b. Suppose each host has two ongoing TCP connections, all to port 80 at host 128.119.40.86. Provide the six corresponding entries in the NAT translation table.
**WAN side              LAN side**
24.34.112.235.4000	    192.168.1.1.3345
24.34.112.235.4001	    192.168.1.1.3346
24.34.112.235.4002	    192.168.1.1.3345
24.34.112.235.4003	    192.168.1.1.3346
24.34.112.235.4004	    192.168.1.1.3345
24.34.112.235.4005	    192.168.1.1.3346

#### P19. Suppose you are interested in detecting the number of hosts behind a NAT. You observe that the IP layer stamps an identification number sequentially on each IP packet. The identification number of the first IP packet generated by a host is a random number, and the identification numbers of the subsequent IP packets are sequentially assigned. Assume all IP packets generated by hosts behind the NAT are sent to the outside world.
##### a. Based on this observation, and assuming you can sniff all packets sent by the NAT to the outside, can you outline a simple technique that detects the number of unique hosts behind a NAT? Justify your answer.
Yes, by observing the numbers and increments of one. If there are jumps in the sequence, that would indicate that there are multiple hosts. This would work for smaller sizes of packets-sent because eventually numbers will interject with eachother.

##### b. If the identification numbers are not sequentially assigned but randomly assigned, would your technique work? Justify your answer.
This technique would not work if the numbers were not ordered sequentially, but assigned randomly. The biggest aspect that allowed the previous technique to work is because the numbers *were assigned in order*. If there are multiple jumps now, we would not which numbers are related by individual hosts anymore.
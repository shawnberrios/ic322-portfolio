# Week 8: More Network Layer: More Data Plane

This week introduces more about the network layer, specifically the data plane!

* [Fortnite Duos](https://gitlab.usna.edu/M1Chen1/ic322-portfolio/-/blob/master/week8/Lab08.md?ref_type=heads)
* [Feedback](https://gitlab.com/owenpitch4d/ic322-portfolio/-/issues/13)
* [I ate my vegetables](week/8/vegetables.md)
* [Learning Objectives](week/8/learning-objectives.md)
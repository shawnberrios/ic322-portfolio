# Learning Objectives

#### LO1. I can explain the problem that NAT solves, and how it solves that problem.
NAT resolves the challenges posed by the limited availability of IPv4 addresses and the necessity to allocate increasingly larger contiguous IP address blocks to a home or small office network to accommodate the addition of devices. In such a scenario, a homeowner must manually manage IP addresses since there is no automated way to keep track of them.

The 10.0.0.0/8 address space constitutes one of three segments reserved for private networks or realms employing private addresses. In a realm utilizing private addresses, these addresses exclusively hold significance for devices within that specific network. This is particularly noteworthy because 10.0.0.0/24 is a widely used address space within private networks. However, since none of these IP addresses are accessible to devices beyond the network, NAT-enabled routers play a crucial role in bridging the gap between private devices and the internet.

To the global internet, a NAT-enabled router presents itself as a single device with a single IP address. For the transmission and reception of data packets, the router assigns random port numbers to a specific IP address and port associated with a device on its network, preserving this association within a translation table. When an IP address with the same random port re-emerges, the router consults the NAT translation table to replace the IP address and port number with those that truly correspond to the intended destination device for the packets. When a home router has NAT enabled, it employs the router's outgoing IP address(es) as the source address for all traffic originating from any device within the home network when connecting to the broader internet. This makes the internet perceive the home network as a single entity. Consequently, incoming traffic from the internet uses the router's outgoing IP address(es) as the destination address. To determine where to route incoming packets, the router relies on a NAT translation table, which includes both port numbers and IP addresses. This table maps public IP address and port combinations to private IP address and port combinations associated with a specific device within the private network.

#### LO2. I can explain important differences between IPv4 and IPv6.
The most notable contrast between IPv4 and IPv6 lies in their address sizes. IPv4, with its 32-bit length, can accommodate approximately 4 billion addresses. In stark contrast, IPv6 boasts an address pool vast enough for every grain of sand to possess a unique address, totaling around 340 trillion addresses. To put this into perspective, one trillion seconds equate to slightly less than 32,000 years, underscoring the sheer scale of IPv6's address space. The reasoning for needing another addressing type, IPv6, is because IPv4 did not provide enough address space for the increasing amount of users.

Other differentiating features encompass anycast addresses, a more compact header size, and the introduction of flow labeling.

Anycast address: This attribute permits a datagram to be dispatched to any one among a cluster of hosts sharing the same address.

Streamlined header size: IPv6 employs a fixed 40-bit header size, ensuring uniformity in dimension, which, in turn, facilitates faster data processing.

Flow labeling: Although somewhat loosely defined, this concept delineates that if data traffic belongs to a specific flow, it might be accorded differing priorities during transmission compared to other flows.

IPv6 incorporates fields that are absent in the IPv4 datagram, including Traffic class (analogous to TOS in IPv4), Flow label, and Payload Length.

Conversely, IPv4 contains fields that do not feature in IPv6, such as fragmentation/reassembly, header checksum, and options.

#### LO3. I can explain how IPv6 datagrams can be sent over networks that only support IPv4.
When introducing IPv6 into the internet, a "flag day" approach, where all existing IPv4 interfaces switch to IPv6 simultaneously, is not feasible. The integration of IPv6 must be a gradual process, involving the addition of IPv6-capable devices while establishing mechanisms for compatibility with existing IPv4 routes.

A widely adopted strategy for achieving this compatibility is known as tunneling. The core concept of tunneling is as follows: Suppose two IPv6 nodes need to exchange IPv6 datagrams, but the only connection between them involves traversing IPv4 routers. This collection of IPv4 routers is commonly referred to as a "tunnel." On the sending end of the tunnel, the IPv6 node encapsulates the IPv6 datagram within the data field of an IPv4 datagram, transmitting it through the tunnel. Upon reaching the tunnel's endpoint, the receiving IPv6 node identifies the presence of an IPv6 datagram within the data of the IPv4 datagram, extracts it, and forwards it to its intended destination.
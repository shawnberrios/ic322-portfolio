# Wireshark Lab: IP v8.0

## Introduction

The purpose of this lab is to investigate the IP protocol, focusing on the specifics of various fields in the IP datagram, and study fragmentation in detail.

## Process

Initially, I followed the link on the course website, but it would not open in my Microsoft Word because it had some outdated feautures, "You are attempting to open a file type (Word 2007 and later Binary Documents and Templates) that has been blocked by your File Block settings in the Trust Center." Even after enabling those files to open, it still wouldn't. So, instead, I followed the instructions entirely from this URL address, [click me!](http://www-net.cs.umass.edu/wireshark-labs/Wireshark_IP_v8.0.pdf) (version 8.0). The v8.1 still would not open, so the v8.0 will suffice.

## 1. Capturing packets from an execution of traceroute

Again, I was unable to use my own sample, so I will have to use the trace file that is provided. Evidence is below.

![PingPlotter](week/7/pingplotter.PNG)
  
![nopackets](week/7/nopackets.PNG)

## 2. A look at the captured trace

#### 1. Select the first ICMP Echo Request message sent by your computer, and expand the Internet Protocol part of the packet in the packet details window. What is the IP address of your computer?

The IP address of my computer is `192.168.1.102`.

![1](week/7/1.PNG)

#### 2. Within the IP packet header, what is the value of the upper layer protocol field?

The value of the upper layer protocol field is `ICMP`. This can be seen in the same image as the previous question on line 8 under the `Protocol` column.

#### 3. How many bytes are in the IP header? How many bytes are in the payload *of the IP datagram*? Explain how you determined the number of payload bytes.

The total length of the data is 56 bytes, but the length of the header is `20` bytes. This means that the payload is *56-20* bytes, which is `36`.

#### 4. Has this IP datagram been fragmented? Explain how you determined whether or not the datagram has been fragmented.

`No`. If the IP datagram fragmented, the more fragments bit should not be **0**.

#### 5. Which fields in the IP datagram always change from one datagram to the next within this series of ICMP messages sent by your computer?

The `Identification`, `Time to Live`, and `Header Checksum` always change from one datagram to the next within this series of ICMP messsages sent by my computer.

#### 6. Which fields stay constant? Which of the fields must stay constant? Which fields must change? Why?

The fields that always stay constant are `Version`, `Header Length`, `Source IP`, `Destination IP`, `Upper Layer Protocol`. Of those, they *must* all stay constant too. This is because of various reasons. The version must remain the same since IPv4 is being used for all packets. The header length must stay the same because all of the packets are following the ICMP protocol. The source IP address because all of the packets are being sent from the same source; same as destination (all packets are being sent to the same source). And of course, the Upper Layer Protocol because all of the packets are ICMP. The fields that must change can be found in the last question: `Identification`, `Time to live`, and `Header checksum`. The packets must have different IDs (they are not the same packet). The time to live is incremented by traceroute for each subsequent packet. The header checksum must change every time the header changes.

#### 7. Describe the pattern you see in the values in the Identification field of the IP datagram

The identification field of the IP datagram increments with Echo (ping) request.

![7](week/7/7.PNG)

#### 8. What is the value in the Identification field and the TTL field?

The Identification field is `42507` and the TTL field is `244`.

#### 9. Do these values remain unchanged for all of the ICMP TTL-exceeded replies sent to your computer by the nearest (first hop) router? Why?

The Identification field changed for all ICMP TTL-exceeded replies since IDs are unique values. If there are datagrams that have the same ID value, it is because they are both fragments of a single larger IP datagram. The TTL field did not change.

#### 10. Find the first ICMP Echo Request message that was sent by your computer after you changed the Packet Size in pingplotter to be 2000. Has that message been fragmented across more than one IP datagram? [Note: if you find your packet has not been fragmented, you should download the zip file http://gaia.cs.umass.edu/wireshark-labs/wireshark-traces.zip and extract the ipethereal-trace-1packet trace. If your computer has an Ethernet interface, a packet size of 2000 should cause fragmentation.3]

###### Note: The packets in the ip-ethereal-trace-1 trace file in http://gaia.cs.umass.edu/wireshark-labs/wiresharktraces.zip are all less that 1500 bytes. This is because the computer on which the trace was gathered has an Ethernet card that limits the length of the maximum IP packet to 1500 bytes (40 bytes of TCP/IP header data and 1460 bytes of upper-layer protocol payload). This 1500 byte value is the standard maximum length allowed by Ethernet. If your trace indicates a datagram longer 1500 bytes, and your computer is using an Ethernet connection, then Wireshark is reporting the wrong IP datagram length; it will likely also show only one large IP datagram rather than multiple smaller datagrams.. This inconsistency in reported lengths is due to the interaction between the Ethernet driver and the Wireshark software. We recommend that if you have this inconsistency, that you perform this lab using the ip-ethereal-trace-1 trace file.

Yes, the packet was fragmented across more than one datagram. This is because when the packet size window was set to 2000, packets needed to be broken up to be sent.

#### 11. Print out the first fragment of the fragmented IP datagram. What information in the IP header indicates that the datagram been fragmented? What information in the IP header indicates whether this is the first fragment versus a latter fragment? How long is this IP datagram?

![11](week/7/11.PNG)

The first fragment is denoted in its header with `"Fragmented IP protocol"`. I know this is the first fragment because I started looking for that header starting at the beginning of the trace. Additionally, the more fragments bit is set to `1`, indicating there are subsequent fragments. The IP datagram is 1500 bytes.

#### 12. Print out the second fragment of the fragmented IP datagram. What information in the IP header indicates that this is not the first datagram fragment? Are the more fragments? How can you tell?

![12](week/7/12.PNG)

The second fragment can be found by annotating fragment offset. The next fragment would have an offset of 1480, since *1500(Total length)-20(Header Length)=1480*. Additionally, the more fragments bit should be set to `0`.

#### 13. What fields change in the IP header between the first and second fragment?

The `total length`, `flags`, and `fragment offset`, and checksum all change from the first and second segment.

#### 14. How many fragments were created from the original datagram?

There are `3` packets that are created from the original datagram.

#### 15. What fields change in the IP header among the fragments?

The `fragment offset` and `checksum` differ among the fragments. The first two packet have a length of 1500 and the more fragments flag is set to 1, while the third packet has a length of 540 and the flag set to 0.
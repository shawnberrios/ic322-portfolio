# Week 7: The Network Layer: Data Plane

This week introduces the data plane of the Network layer of the 5-layered Internet model.

* [Main](week/7/wireshark.md)
* [I ate my vegetables](week/7/vegetables.md)
* [Learning Objectives](week/7/learning-objectives.md)
# Kurose, Chapter 4 Questions

#### R11. Describe how packet loss can occur at input ports. Describe how packet loss at input ports can be eliminated (without using infinite buffers).

Input buffer space could be exhausted if the switching fabric is slow. When N is the number of ports, the switching fabric must be at least N times as fast as the input line speed. This is not true for a crossbar fabric.

#### R12. Describe how packet loss can occur at output ports. Can this loss be prevented by increasing the switch fabric speed?

Overspeeding the switching fabric could cause packets to be lost in the output buffer if the fabric speed is greater than the line speed on all output ports.

#### R13. What is HOL blocking? Does it occur in input ports or output ports?

Providing the packets have different output ports, the switching fabric can transfer packets in parallel. The first packet in the input queue that has the same output port number will be blocked and have to wait at the input queue if there are two packets with the same output port number at the front of two input queues. In HOL blocking, packets queued behind a packet waiting for an output are also blocked, and they must wait for the packet in front to clear.

#### R16. What is an essential different between RR and WFQ packet scheduling? Is there a case (Hint: Consider the WFQ weights) where RR and WFQ will behave exactly the same?

In RR (Round Robin) scheduling, the packets are classified into classes and rotated through a scheduler between these classes in order to avoid starvation of particular packet; in RR, all packets get equal priority. In WFQ (Weighted Fair Queueing) scheduling, packets are classified into classes and the scheduler will rotate among them, based on a weight assigned to them. The case where RR and WFQ behave equally is when the scheduler operating under WFQ assigns equal weights to all packets, thus emulating a RR-type scheduling system.

#### R18. What field in the IP header can be used to ensure that a packet is forwarded through no more than *N* routers?

Time-to-live field. This field indicates how long a packet can survive.

#### R21. How many IP addresses does a router have?

Routers possess a unique characteristic as they are equipped with two IP addresses. Each of these IP addresses is allocated to one of the router's two "interfaces." The initial interface is referred to as the WAN (Wide Area Network) interface, which is the side of the router that interfaces with the Internet and bears a public IP address. The second IP address is associated with the corresponding router interface.

#### P4. Consider the switch shown below. Suppose that all datagrams have the same fixed length, that the switch operates in a slotted, synchronous manner, and that in one time slot a datagram can be transferred from an input port to an output port. The switch fabric is a crossbar so that at most one datagram can be transferred to a given output port in a time slot, but different output ports can receive datagrams from different input ports in a single time slot. What is the minimal number of time slots needed to transfer the packets shown from input ports to their output ports, assuming any input queue scheduling order you want (i.e., it need not have HOL blocking)? What is the largest number of slots needed, assuming the worst-case scheduling order you can devise, assuming that a non-empty input queue is never idle?

![P4](week/7/p4.PNG)

The minimal number of time slots needed to transfer the packets shown from the input ports to their output ports is `3`. This is because in the first slot, "X" will be sent in the top input queue, with "Y" in the middle input queue. In slot 2, "X" will be sent in the middle queue, with "Y" in the bottom input queue. In the last slot, "Z" will be sent in the bottom input queue. The largest number of slots needed, assuming the worst-case scheduling order and that the input queue is never idle, despite being non-empty, is `4`. The reason for this is because if "X" is sent in the bottom input queue of slot 1, then an extra slot would be needed to transmit all of the packets. 

#### P5. Suppose that the WEQ scheduling policy is applied to a buffer that supports three classes, and suppose the weights are 0.5, 0.25, and 0.25 for the three classes.
##### a. Suppose that each class has a large number of packets in the buffer. In what sequence might the three classes be served in order to achieve the WFQ weights? (For round robin scheduling, a natural sequence is 123123123 . . .).
  
`1 2 1 3 1 2 1 3`  
  
##### b. Suppose that classes 1 and 2 have a large number of packets in the buffer, and there are no class 3 packets in the buffer. In what sequence might the three classes be served in to achieve the WFQ weights?
  
`1 1 2 1 1 3 1 1 2 1 1`  
  
#### P8. Consider a datagram network using 32-bit host addresses. Suppose a router has four links, numbered 0 through 3, and packets are to be forwarded to the link interfaces as follows:

    Destination Address Range            Link Interface  
11100000 00000000 00000000 00000000  
            through                            0  
11100000 00111111 11111111 11111111  
  
11100000 01000000 00000000 00000000  
            through                            1  
11100000 01000000 11111111 11111111  
  
11100000 01000001 00000000 00000000  
            through                            2  
11100001 01111111 11111111 11111111  
            otherwise                          3  
  
##### a. Provide a forwarding table that has five entries, uses longest prefix matching, and forwards packets to the correct link interfaces.
  
Prefix Match        Link Interface  
  
11100000            00 
11100000 01000000   1  
1110000             2  
11100001 1          3  
otherwise           3  
  
##### b. Describe how your forwarding table determines the appropriate link interface for datagrams with destination addresses:
  
11001000 10010001 01010001 01010101  *Matches to otherwise*  
11100001 01000000 11000011 00111100  *Matches to Link 2*  
11100001 10000000 00010001 01110111  *Matches to Link 3*  
  
#### P9. Consider a datagram network using 8-bit host addresses. Suppose a router uses longest prefix matching and has the following forwarding table:
  
Prefix Match Interface  
00           0  
010          1  
011          2  
10           2  
11           3  

#### For each of the four interfaces, give the associated range of destination host addresses and the number of addresses in the range.

**Destination Range Link Interface**  
00000000  
through 0  
00111111  
  
*2^6=64 addresses*  
  
01000000  
through 1  
01011111  
  
*2^5=32 addresses*  
  
01100000  
through 2  
01111111  
  
10000000  
through 2  
10111111  
  
*2^6+2^5=96 addresses*  
  
11000000  
through 3  
11111111  
  
*2^6 = 64 addresses*  
  
#### P11. Consider a router that interconnects three subnets: Subnet 1, Subnet 2, and Subnet 3. Suppose all of the interfaces in each of these three subnets are required to have the prefix 223.1.17/24. Also suppose that Subnet 1 is required to support at least 60 interfaces, Subnet 2 is to support at least 90 interfaces, and Subnet 3 is to support at least 12 interfaces. Provide three network addresses (of the form a.b.c.d/x) that satisfy these constraints.

Three network addresses that would satisfy these constraints are:

`223.1.17.0/26`  
`223.1.17.128/25`  
`223.1.17.192/28`  
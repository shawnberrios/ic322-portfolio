# Wireshark Lab: DNS v8.0

## Introduction

The purpose of this lab is to take a closer look at the client side of DNS. DNS translates hostnames to IP addresses and a lot of what happens is "under the covers" which is essentially invisible to DNS clients, as the hierarchical DNS servers communicate with each other to either recursively or iteratively resolve the client's DNS query. From the DNS client's side, the process is relatively simple -- a *query* is formulated to the local DNS server and a *response* is received from that server.

## Process

I simply followed the instructions entirely from the listed URL address, [click me!](http://www-net.cs.umass.edu/wireshark-labs/Wireshark_DNS_v8.0.pdf) (version 8.0). I could not use the latest version of the lab from the *Kurose* website (v8.1), so the v8.0 will suffice.

I was also unable to follow with my own system due to network difficulties. In Kurose's Computer Networking, a zip file is provided with multiple trace files. I will be walking through the PDF from above to guide me through answering the questions.

## 1. nslookup

#### 1. Run *nslookup* to obtain the IP address of a Web server in Asia. What is the IP address of that server?

I used nslookup on [tengine.taobao.org](tengine.taobao.org).

![1-1](week/3/1-1.PNG)

#### 2. Run *nslookup* to determine the authoritative DNS servers for a university in Europe.

I used nslookup on [https://www.ox.ac.uk/](https://www.ox.ac.uk/).

![1-2](week/3/1-2.PNG)

#### Run *nslookup* so that one of the DNS servers obtained in Question 2 is queried for the mail servers for Yahoo! mail. What is its IP address?

I inputted the command 'nslookup www.ox.ac.uk mail.yahoo.com'

The IP address is `69.147.92.11`.

![1-3](week/3/1-3.PNG)

## 2. ipconfig

![ipconfig](week/3/ipconfig_all.PNG)

*What happens after typing 'ipconfig /all'*

## 3. Tracing DNS with Wireshark

#### 4. Locate the DNS query and response messages. Are then sent over over UDP or TCP?

After locating the DNS query and response messages, I determined that they were sent over UDP.

![3-4](week/3/3-4.PNG)

#### 5. What is the destination port for the DNS query message? What is the source port of DNS response message?

First of all, the destination port for the DNS query message and source port for the DNS response message will be the same. The port is 53.

#### 6. To what IP address is the DNS query message sent? Use ipconfig to determine the IP address of your local DNS server. Are these two IP address the same?

I am unable to provide an answer for this question as I am following the traced file that was provided in the textbook. I would assume that the two IP addresses are the same, especially since the query message is being sent to one of my local DNS servers.

#### 7. Examine the DNS query message. What "type" of DNS query is it? Does the query message contain any "answers"?

The query message is a standard message. This can be found at navigating to 'Domain Name System' and hitting the 'Flags' dropdown.

![3-7](week/3/3-7.PNG)

#### 8. Examine the DNS response message. How many "answers" are provided? What do each of these answers contain?

The response message was also a standard query response, but instead, contained 2 answers. Both of the answers were type A, class IN, but had different addresses.

![3-8](week/3/3-8.PNG)

#### 9. Consider the subsequent TCP SYN packet sent by your host. Does the destination IP address of the SYN packet correspond to any of the IP addresses provided in the DNS response message?

The destination of the IP address of the SYN packet does *not* correspond to any of the IP addresses in the response message. The destination IP address of the SYN packet was `132.151.6.75`, and doesn't match to neither the source or destination IP of the query response.

#### 10. This web page contains images. Before retrieving each image, does your host issue new DNS queries?

If the web page contains images that are stored in the cache, the host does *not* issue new DNS queries. This way, so that network traffic can be minimized and efficiency is optimized. If the web page images are in the cache, they are then downloaded locally; no need to issue new DNS queries.

#### 11. What is the destination port for the DNS query message? What is the source port of DNS response message?

The destination port for the DNS query message and the source port of the DNS response messsage are both `53`.

![3-11](week/3/3-11a.PNG)

![3-11](week/3/3-11b.PNG)

#### 12. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?

Again, I am unable to provide this answer as I am following a trace file. I would assume yes.

#### 13. Examine the DNS query message. What "Type" of DNS query is it? Does the query message contain any "answers"?

The DNS query message is a standard query, does query recursively, and does not contain any "answers".

`Answers RRs: 0`

#### 14. Examine the DNS response message. How many "answers" are provided? What do each of these answers contain?

The DNS response message is also a standard query response message. It allows recursion for the server as well. There is one answer that is provided. It says "Answer RRs: 1," and goes on to explain the answer, "Answer/authority" portion was not authenticated by the server.

#### 15. Provide a screenshot.

![3-15](week/3/3-15.PNG)

### Now with the command `nslookup -type=NS mit.edu`

#### 16. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?

Again, I am unable to provide this answer as I am following a trace file. I would assume yes.

#### 17. Examine the DNS query message. What "Type" of DNS query is it? Does the query message contain any "answers"?

The DNS query message is standard and does not contain any answers.

#### 18. Examine the DNS response message. What MIT nameservers does the response message provide? Does this response message also provide the IP addresses of the MIT nameservers.

There are three MIT nameservers that are provided in the response message. This can be found by navigating to the 'Domain Name System (response)' drop-down located on the bottom left and then hitting the drop down of 'Additional records'. The three that are provided are listed below with corresponding IP addresses.

`bitsy.mit.edu: type A, class IN, addr 18.72.0.3`\
`strawb.mit.edu: type A, class IN, addr 18.71.0.151`\
`w20ns.mit.edu: type A, class IN, addr 18.70.0.160`

#### 19. Provide a screenshot.

![3-19](week/3/3-19.PNG)

### Now with the command `nslookup www.aiit.or.kr bitsy.mit.edu`

#### 20. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server? If not, what does the IP address correspond to?

Again, I am unable to provide this answer as I am following a trace file. I would assume yes.

#### 21. Examine the DNS query message. What "Type" of DNS query is it? Does the query message contain any "answers"?

The DNS query message is a standard message and does not contain any "answers".

#### 22. Examine the DNS response message. How many "answers" are provided? What does each of these answers contain?

After examing the DNS response message, I was able to note that *one* answer was provided with it. The answer contained:\

'www.aiit.or.kr: type A, class IN, addr 218.36.94.200'

#### 23. Provide a screenshot.

![3-25](week/3/3-25.PNG)
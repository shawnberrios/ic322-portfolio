# Week 3: Application Layer (Other than HTTP)

This week expands the concept of the Application Layer and introduces DNS and other protocols.

* [Main](wireshark.md)
* [I ate my vegetables](week/3/vegetables.md)
* [Learning Objectives](week/3/learning-objectives.md)
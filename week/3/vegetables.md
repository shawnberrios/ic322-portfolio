# Kurose, Chapter 3 Questions

If I ever want to write my own SMTP server, use [this](https://stackoverflow.com/questions/40059025/proper-response-to-smtp-helo).

#### R16. Suppose Alice, with a Web-based e-mail account (such as Hotmail or Gmail), sends a message to Bob, who accesses his mail from his mail server using IMAP. Discuss how the message gets from Alice's host to Bob's host. Be sure to list the series of application-layer protocols that are used to move the message between the two hosts.

The main purpose of **SMTP** is to to push e-mail from one host to another. In a simple Bob and Alice example, we can see the path an e-mail must take when going from one person to another. Alice's user agent uses SMTP to push the e-mail message into her own mail server. After it reaches Alice's mail server, it uses SMTP (as the client) to relay the e-mail message to Bob's mail server. This process involves two steps primarily because withot relaying through Alice's mail server, Alice's user agent doesn't have any recourse to hit an unreachable destination mail server. By first dropping the e-mail into her own server, she is equipped to continue trying Bob's mail server until his server becomes operational. One's the e-mail reaches Bob's mail server, he must use something other than SMTP, because, after all, SMTP is a push protocol. So in order for Bob, who is a running agent on his local PC, to obtain his messages, which are sitting in a mail server within Bob's ISP, Bob must use a **Post Office Protocol (POP)**. POP3 is a special mail access protocol that transfers messages from Bob's mail server to his local PC. There are multiple POPs, including Version 3 (POP3), **Internet Mail Access Protocol (IMAP)**, and HTTP.

![LO3](week/3/lo3.PNG)

#### R18. What is the HOL blocking issue in HTTP/1.1? How does HTTP/2 attempt to solve it?

HOL blocking is an issue in HTTP/1.1 and it stands for head-of-the-line blocking in an input ending switch, in which a queued packet in an input queue must wait for transfer through the fabric (even though its output port is free) because it is blocked by another packet at the head of the line. StackOverflow describes HOL blocking as when each browser/client has a limited amount of connections to a server and doing a new request over one of those connections has to wait for the ones to complete before it can fire off. When HOL blocking occurs, it is possible that packets will be dropped and lossed, especially if the input queue grows to an unbounded length. HTTP/2 solves this issue by using a method called multiplexing, so that new requests can be issued over the same connection without having to wait for previous ones to complete. It is important to note that the question states, "How does HTTP/2 *attempt* to solve it," meaning that it still has its drawbacks. Although HTTP/2 does not change the semantics of HTTP, this version of HTTP still has issues, namely at the TCP level. One lost packet in the TCP stream makes all streams wait until that packet is re-transmitted and received [StackOverflow](https://stackoverflow.com/questions/45583861/how-does-http2-solve-head-of-line-blocking-hol-issue).

![LO3](week/3/http-versions.PNG)

[The Curious Case of Parallel Connections in HTTP/2](https://dl.ifip.org/db/conf/cnsm/cnsm2016/1570287809.pdf)

#### R24. CDNs typically adopt one of two different server placement phiolosophies. Name and briefly describe them.

There are two types of server placement philosophies when it comes to Content Distribution Networks (CDNs). CDNs install many geographically distributed caches throughout the Internet, localizing traffic to be closer to its users. The first type of server placement is through **shared** CDNs. If a CDN adopts a 'shared' server philosophy and the user requests data, that data is relayed through a bunch of servers. The data is then cached and transmitted through many servers that are strategically placed around the world. It allows data to be consumed by multiple users simultaneously. Large companies like Netflix, Hulu, and eBay adopt this system, as it is efficient and common for multiple users to be browsing the same thing. The second type are called **dedicated** CDNs. These servers are more expensive, but they have more specialized roles. They are also used in conjunction with more CDNs. An example of a dedicated CDN is if a company wanted to bring their website closer to a potential market, while at the same time, there is another one that is for assisting remote coworkers [BelugaCDN](https://www.belugacdn.com/dedicated-cdn/). Dedicated servers usually operate private CDNs, which offer streaming services to users. These streaming services have to pay for not only the server hardware, but the bandwidth too, in order to distribute videos.

#### P16. How does SMTP mark the end of a message body? How about HTTP? Can HTTP use the same method as SMTP to mark the end of a message body? Explain.

SMTP and HTTP both utilize different things to indicate the end of a message body, "designates the end of message with an isolated period, and issues `QUIT` only after all messages have been sent" (153). SMTP uses a line containing only a period, while HTTP does not. HTTP consists of a carriage and line feed at the end of its message. They cannot use the same notation. This is because SMTP utilizes the 7-bit ASCII format, while HTTP could be in binary.

An example transcript of an exchange of messages in an SMTP client provided by the book:

S: 220 hamburger.edu\
C: HELO crepes.fr\
S: 250 Hello crepes.fr, pleased to meet you\
C: MAIL FROM: <alice@crepes.fr>\
S: 250 alice@crepes.fr ... Sender ok\
C: RCPT TO: <bob@hamburger.edu>\
S: 250 bob@hamburger.edu ... Recipient ok\
C: DATA\
S: 354 Enter mail, end with ”.” on a line by itself\
C: Do you like ketchup?\
C: How about pickles?\
C: .\
S: 250 Message accepted for delivery\
C: QUIT\
S: 221 hamburger.edu closing connection

#### P18. a. What is a *whois* database? b. Use various whois databases on the Internet to obtain the names of two DNS servers. Indicate which whois databases you used.

A *whois* database is simply a telephone book that associates information to specific domain names. More specifically, it is a query and response protocol that is used for querying databases which store the Internet's resources registered users, including IP address blocks and autonomous systems. Autonomous Systems are collections of IP routing prefixes that allow Internet connected systems to intercommunicate. You can use ASN, autonomous System Numbers for IP prefixes, or you can lookup IP addresses to get details of an autonomous system.

minecraft.net - whois.markmonitor.com\
google.com - whois.markmonitor.com

#### P20. Suppose you can access the caches in the local DNS servers of your department. Can you propose a way to roughly determine the Web servers (outside your department) that are most popular among the users in your department? Explain.

I suppose that it would be easy to determine the most popular Web servers among the local DNS servers if I had access to the caches. An important feature of previously visited Web sites is that instead of traditional GET requests, conditional GET requests are sent. Within a conditional GET request, you are able to see the date that Web server was last updated. Instead of downloading the same data again, if the data is up-to-date, the data will be pulled from local storage. To determine which Web servers are most popular, I can view the dates each of them were last modified. If it was recent, it would seem that it could be popular as well.

#### P21. Suppose that your department has a local DNS server for all computers in the department. You are an ordinary user (i.e., not a network/system administrator). Can you determine if an external Web site was likely accessed from a computer in your department a couple of seconds ago. Explain.

Yes, the tool `dig` that was explored in [here](week/3/learning-objectives.md) helps up to do just that! The reason being because we are in the local DNS server. If we were to do something like `dig minecraft.net`, the query time it took to find 'minecraft.net' will return. If 'minecraft.net' was accessed only a couple seconds ago, an entry of it was cached in the local DNS cache. If not, it has to find it, which is the difference of a query time of 0ms and a lot!
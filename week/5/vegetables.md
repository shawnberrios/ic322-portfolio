# Kurose, Chapter 3 Questions

#### R17. Suppose two TCP connections are present over some bottleneck link of rate *R* bps. Both connections have a huge file to send (in the same direction over the bottleneck link). The tranmissions of the files start at the same time. What transmission rate would TCP like to give each of the connections?

Since the transmissions start at the same time, it makes sense that the rate would be *R*/2.

#### R18. True or false? Consider congestion control in TCP. When the timer expires at the sender, the value of `ssthresh` is set to one half of its previous value.

False. The timer is sent to half of the current value of the congestion window, not half of its previous size.

#### P27. Host A and B are communicating over a TCP connection, and Host B has already received from A all bytes up through byte 126. Suppose Host A then sends two segments to Host B back-to-back. The first and second segments contain 80 and 40 bytes of data, respectively. In the first segment, the sequence number is 127, the source port number is 302, and the destination port is 80. Host B sends an acknowledgement whenever it receives a segment from Host A.

##### a. In the second segment sent from Host A to B, what are the sequence number, source port number, and destination port number?

**Sequence number** - `207` *(127 + 80)*
**Source port number** - `302`
**Destination port number** - `80`

##### b. If the first segment arrives before the second segment, in the acknowledgement of the first arriving segment, what is the acknowledgement number, the source port number, and the destination port number?

**Sequence number** - `207` *(Acknowledgment number if first segment arrives before second)*
**Source port number** - `80`
**Destination port number** - `302`

##### c. If the second segment arrives before the first segment, in the acknowledgment of the first arriving segment, what is the acknowledgement number?

**Acknowledgment number** - `127`

##### d. Suppose the two segments sent by A arrive in order at B. The first acknowledgment is lost and the second acknowledgment arrives after the first timeout interval. Draw a timing diagram, showing these segments and all other segments and acknowledgments sent. (Assume there is no additional packet loss.) For each segment in your figure, provide the sequence number and the number of bytes of data; for each acknowledgment that you add, provide the acknowledgment number.

![p27](week/5/p27.PNG)

#### P33. In Section 3.5.3, we discussed TCP's estimation of RTT. Why do you think TCP avoids measuring the `SampleRTT` for retransmitted segments?

TCP avoids measuring the `SampleRTT` for retransmitted segments because it could mistakenly measure a delayed ACK and calculate an incorrect value for the SampleRTT. Let's explore an example. Imagine a source sends packet 1. Shortly after, the timer expires and so the source sends packet 2, which is a copy of packet 1. Suppose the source measures the SampleRTT for packet 2. Now suppose that the acknowledgement for 1 finally arrives. The source will mistakenly measure the acknowledgement for 2, which will not be the correct value for the SampleRTT.

#### P36. In Section 3.5.4, we saw that TCP waits until it has received three duplicate ACKs before performing a fast retransmit, Why do you think the TCP designers chose not to perform a fast retransmit after the first duplicate ACK for a segment is received?

If TCP designers chose to perform fast retransmits after the first duplicate ACKs, it could lead to packets sent from the Internet Protocol layer to be out-of-order. Additionally, if out-of-order packets arrive, the receiver immediately sends a duplicate ACK for the already received segment. Senders usually sent more than one segment back to back and if a segment is lost, there could be multiple back-to-back ACKs. So, the designers decided to wait for the third acknowledgment to avoid redundant packet transmission.

#### P40. Consider Figure 3.58 . Assuming TCP Reno is the protocol experiencing the behavior shown above, answer the following questions. In all cases, you should provide a short discussion justifying your answer.

![P40](week/5/P40.PNG)

##### a. Identify the intervals of time when TCP slow start is operating.

TCP slow start happens throughout the transmission rounds `1` to `6` and `23` to `26`.

##### b. Identify the intervals of time when TCP congestion avoidance is operating.

TCP congestion avoidance is operating between `6` to `16` and `17` to `22`.

##### c. After the 16th transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?

After the 16th transmission round, segment loss is denoted by a *triple duplicate ACK*. If there were a timeout, the congestion window would have looked like transmission round 23.

##### d. After the 22nd transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?

After the 22nd transmission, segment loss is denoted by a *timeout*, which is why the congestion window dropped to a size of 1 segment.

##### e. What is the initial value of ssthresh at the first transmission round?

The initial value of ssthresh at the first transmission round is `32`. This can be evaluated because in the graph, after transmission round 32, there is some sort of control mechanism that is limiting the expansion of the window size (more specifically, TCP congestion control).

##### f. What is the value of ssthresh at the 18th transmission round?

The value of ssthresh at the 18th tranmission round is `21`, which can be observed from the graph, but also by dividing the congestion window size in half. When packet loss is detected, the new threshold is now half of what the congestion window was. Thus, 42/2=21.

##### g. What is the value of ssthresh at the 24th transmission round?

The value of the ssthresh at the 24th transmisson round is `13`.

##### h. During what transmission round is the 70th segment sent?

The 70th segment is sent during the *7th* transmission round.

##### i. Assuming a packet loss is detected after the 26th round by the receipt of a triple duplicate ACK, what will be the values of the congestion window size and of ssthresh ?

The values of the congestion window size and ssthresh after the 26th round will be `4` and `7`.

##### j. Suppose TCP Tahoe is used (instead of TCP Reno), and assume that triple duplicate ACKs are received at the 16th round. What are the ssthresh and the congestion window size at the 19th round?

If TCP Tahoe is used instead of TCP Reno, the ssthresh and congestion window at the 19th round will be `21` *(42/2)* and `1`.

##### k. Again suppose TCP Tahoe is used, and there is a timeout event at 22nd round. How many packets have been sent out from 17th round till 22nd round, inclusive?

If TCP Tahoe is used and there is a timeout event at the 22nd round, `52` packets will be sent. 

Round 17 = 1 packet
Round 18 = 2 packets
Round 19 = 4 packets
Round 20 = 8 packets
Round 21 = 16 packets
Round 22 = 21 packets
---------------------
Total = 52 packets
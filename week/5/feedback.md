# Feedback for [owenpitch4d](https://gitlab.com/owenpitch4d/ic322-portfolio/-/tree/main?ref_type=heads)

## README.md

I like Owen's README because it is simple and clear. It is different than what we were told to make at the beginning, so it shows that he is ambitious and straight-forward. It is better than mine because of how straight-to-the-point it is.

## Review Questions

Owen's answers for the 'Eat your vegetables' were fantastic. His answers were very clear and easy to read. His answers did not raise any questions for me, but they certainly solidified all of mine.

## Learning Goals

Owen did a phenomenal job at articulating what he learned in this chapter. He demonstrated a thorough understanding of the material and went above and beyond what was asked. His explanations of how particular mechanisms are used in TCP helped solidify my own knowledge of the material. He even provided multiple markdown formats that made his portfolio very easy to read. Owen also introduced the concept of Cumulative ACKs, which was not even asked in the question!

## Lab

Owen's lab write-up is very meticulous and follows the format we were required to abide to. He introduces what this week is about, who we collaborated with, the process, and finishes with the questions. Each of his answers are in complete sentences, made pretty with markdown, and all complete. He even provided his own graphs and files for I, as a reader, to follow along. The one thing that I love about Owen's is that he puts his lab in a seperate folder. This keeps everything organized and seperates learning objectives and growing your vegetables to his lab report, which I will incorporate too.

#####

Create a new Issue. Give it a good title, like "Week 4 Peer Feedback". You can use Markdown in your issue to create # Headings or - Bulleted Lists.
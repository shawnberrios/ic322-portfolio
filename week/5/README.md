# Week 5: Transport Layer

This week dives deeper into the Transport Layer and examines TCP and what it entails.

* [Feedback for owenpitch4d](week/5/feedback.md)
* [I ate my vegetables](week/5/vegetables.md)
* [Learning Objectives](week/5/learning-objectives.md)
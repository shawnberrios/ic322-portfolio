# Learning Objectives

#### LO1. I can explain what autonomous systems are and their significance to the Internet.
Autonomous Systems represent clusters of routers operating under a unified administrative domain. These routers employ a shared routing algorithm, typically an intra-AS routing protocol such as OSPF. The grouping of routers into Autonomous Systems serves various functional purposes. For instance, routers within a single Internet Service Provider (ISP) may be organized into a single autonomous system, or an ISP administrator may choose to categorize them into multiple autonomous systems, depending on specific needs and preferences.

Autonomous Systems are distinguished by globally unique Autonomous System Numbers (ASNs), which are allocated by ICANN regional registries in a manner analogous to IP address assignment. Their importance in the context of the Internet lies in the capability they offer organizations to independently manage and oversee their own network operations, thereby safeguarding internal information from the broader Internet, all while maintaining the ability to connect with other networks within the larger Internet.

#### LO2. I can describe how the BGP protocol works as well as why and where it is used.
BGP, or the Border Gateway Protocol, is a crucial routing protocol used in the operation of the global Internet. It plays a central role in ensuring the efficient exchange of routing and reachability information between Autonomous Systems (ASes), which are individual networks or network domains under the control of a single administrative entity. BGP is different from interior gateway protocols (IGPs) like OSPF and EIGRP, which are used within an AS to exchange routing information, whereas BGP is used for routing between ASes.

Here's an in-depth look at how BGP works, why it is essential, and where it is used:

1. Path Vector Protocol:
BGP is a path vector protocol, which means it keeps track of the path that data traverses to reach a destination. Each BGP router maintains a routing table containing information about networks and the paths to reach them. Unlike distance-vector or link-state protocols, BGP routers share only their best paths with their neighbors, making it scalable for the global Internet.

2. Autonomous Systems (ASes):
The Internet is divided into numerous Autonomous Systems, which are individual networks or organizations. Each AS is assigned a unique Autonomous System Number (ASN) by a regional internet registry (RIR). BGP is used to exchange routing information between these ASes. BGP routers within the same AS use IGPs to establish internal routes.

3. BGP Sessions:
BGP routers establish BGP sessions with their neighboring routers in different ASes. These sessions are based on the Transmission Control Protocol (TCP), providing reliability and error-checking. BGP routers exchange routing information through these sessions.

Where BGP Is Used:

**Internet Backbone:** BGP is the routing protocol used by Internet backbone providers and large ISPs to manage the interconnection of networks.
**Enterprise Networks:** Some large organizations use BGP to connect their networks to multiple ISPs for redundancy and load balancing.
**Data Centers:** BGP is used in data center environments to manage network traffic and optimize routing within and between data centers.
**Cloud Services:** BGP is used in cloud services to establish peering relationships with customers, ensuring the efficient routing of traffic to and from the cloud provider's network.

In summary, BGP is a critical protocol for the global Internet, facilitating the exchange of routing information between Autonomous Systems. Its path vector nature, complex decision-making process, and support for routing policies make it suitable for managing the intricate interconnections and routing requirements of the modern internet.

#### LO3. I can explain what the ICMP protocol is used for, with concrete examples.
ICMP (Internet Control Message Protocol) serves as a means of communication between hosts and routers, primarily for the exchange of network layer information, especially error notifications. For instance, in the event that a router cannot locate the designated host for an HTTP session, it generates and transmits an ICMP message to the initial host to convey the error. ICMP messages are encapsulated within IP datagrams and are commonly linked with the IP protocol. The following catalog outlines various other types of ICMP messages.

[ExtendedListOfICMPProtocols](https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol)
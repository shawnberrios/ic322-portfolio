# ICMP Pinger Lab

## Introduction
This lab solidifies the concept of ICMP through ping. Ping is a common term among computers, especially in the gaming world and determines whether a particular host is reachable across an IP network. It works by sending ICMP "echo reply" packets to a targest host and then listening for "echo reply" replies. This lab works to complete my own version of the Ping application so that it sends ping requests to a specified host which are separated by one second.

## Process
The document given in the instructions provided a large framework that I developed off of. The link can be found [here](https://gaia.cs.umass.edu/kurose_ross/programming/Python_code_only/ICMP_ping_programming_lab_only.pdf).

## Collaboration
I had trouble writing the program so I found a resource on [GitLab](https://gist.github.com/abcdabcd987/eaf210249fab605a75bac3409b614372) that helped me finish it.

## Trouble
My computer (any of the government issued computers) have trouble using wireshark, so you have to take my word that my program works.

The code is below.

[Pinger](week/10/pinger.py)
# Week 10: Network Layer: More Control Plane

This week talks about the control plane even more!

* [Main](week/10/ICMPping.md)
* [Partner Feedback](https://gitlab.com/m254890/paratore-ic322-portfolio/-/issues/16)
* [I ate my vegetables](week/10/vegetables.md)
* [Learning Objectives](week/10/learning-objectives.md)
# Kurose, Chapter 5 Questions

#### R11. How does BGP use the NEXT-HOP attribute? How does it use the AS-PATH attribute?
The AS-PATH attribute serves the purpose of identifying and mitigating looped advertisements within routers, while also aiding in route selection when faced with multiple options for a specific prefix. On the other hand, the NEXT-HOP attribute signifies the IP address of the initial router encountered on the advertised path, beyond the autonomous system (AS) that receives the advertisement. When routers set up their forwarding tables, they rely on the NEXT-HOP attribute.

#### R13. True or false: When a BGP router receives an advertised path from its neighbor, it must add its own identity to the received path and then send that new path on to all of its neighbors. Explain.
False. In a policy-based routing protocol like BGP, there is the possibility for a BGP router to decide against including its own identity in the path it receives and then transmit this modified path to all its neighboring routers. This can occur in situations where the received path's destination belongs to a different autonomous system (AS) rather than the AS to which the BGP router belongs, and the BGP router has no intention of serving as a transit router.

#### R19 Names four different types of ICMP messages (also describe what they are used for).
ICMP (Internet Control Message Protocol) is a network layer protocol used in IP networks to communicate error and control messages between network devices. There are four main types of ICMP messages, each serving distinct purposes:

1. **Error Reporting Messages:**
   - **Destination Unreachable:** Sent by a router to indicate that a destination host or network is unreachable.
   - **Time Exceeded:** Used to notify that a packet's time to live (TTL) has expired, preventing it from reaching its destination.
   - **Parameter Problem:** Sent when there is an issue with the IP header or parameters within the IP packet.

2. **Query Messages:**
   - **Echo Request and Echo Reply (Ping):** These messages are used for network troubleshooting and connectivity testing. The sender (Echo Request) requests a response from the recipient (Echo Reply) to verify that a destination host is reachable.

3. **Redirect Messages:**
   - **Redirect Message:** Sent by a router to inform a host to update its routing table, typically to use a better next-hop router for a particular destination.

4. **Informational Messages:**
   - **Timestamp Request and Timestamp Reply:** Used to measure the round-trip time between two hosts.
   - **Address Mask Request and Address Mask Reply:** These messages are used to discover the subnet mask of a network, which can be helpful for hosts configuring their network settings.

ICMP is a fundamental protocol for network troubleshooting, error reporting, and various network management tasks. These message types help network administrators and devices communicate vital information and maintain the reliability and integrity of network communication.

#### R20. What two types of ICMP messages are received at the sending host executing the Traceroute program?
Port unreachable and TTL (Time to Live) Exceeded messages are directed back to the originating host.

In situations where a packet arrives at its destination host, targeting a UDP port without a corresponding host association, it triggers the generation of an ICMP message denoting "port unreachable." Additionally, an ICMP Port Unreachable message can be produced when a router fails to locate the intended server.

On the other hand, TTL expiration comes into play when the traceroute command is executed. An ICMP message is dispatched with a TTL value of 1 set within the IP header. Once a single probe reaches the end host, it generates an ICMP message indicating "TTL Exceeded."

#### P14. Consider the network shown below. Suppose AS3 and AS2 are running OSPF for their intra-AS routing protocol. Suppose AS1 and AS4 are running RIP for their intra-AS routing protocol. Suppose eBGP and iBGP are used for the inter-AS routing protocol. Initially suppose there is no physical link between AS2 and AS4.
##### a. Router 3c learns about prefix x from which routing protocol: OSPF, RIP, eBGP, or iBGP?
eBGP

##### b. Router 3a learns about x from which routing protocol?
iBGP

##### c. Router 1c learns about x from which routing protocol?
eBGP

##### d. Router 1d learns about x from which routing protocol? 
iBGP

#### P15. Referring to the previous problem, once router 1d learns about x it will put an entry (x, I) in its forwarding table.
##### a. Will I be equal to I1 or I2  for this entry? Explain why in one sentence.
Interface I1 initiates the path of minimal cost from 1d toward gateway router 1c.

##### b. Now suppose that there is a physical link between AS2 and AS4, shown by the dotted line. Suppose router 1d learns that x is accessible via AS2 as well as via AS3. Will I be set to I1 or I2? Explain why in one sentence.
I2. Both paths possess identical AS-PATH lengths; however, I2 commences the path that leads to the nearest NEXTHOP router.

##### c. Now suppose there is another AS, called AS5, which lies on the path between AS2 and AS4 (not shown in diagram). Suppose router 1d learns that x is accessible via AS2 AS5 AS4 as well as via AS3 AS4. Will I be set to  or ? Explain why in one sentence.
I1. I1 begins the path that has the shortest ASPATH. 

#### P19. In Figure 5.13, suppose that there is another stub network V that is a customer of ISP A. Suppose that B and C have a peering relationship, and A is a customer of both B and C. Suppose that A would like to have the traffic destined to W to come from B only, and the traffic destined to V from either B or C. How should A advertise its routes to B and C? What AS routes does C receive?
A should aid B with two routes: AS-paths A-W and A-V. A should aid to C only A-V, and C should receive AS paths: B-A-W, B-A-V, and A-V.

#### P20. Suppose ASs X and Z are not directly connected but instead are connected by AS Y. Further suppose that X has a peering agreement with Y, and that Y has a peering agreement with Z. Finally, suppose that Z wants to transit all of Y’s traffic but does not want to transit X’s traffic. Does BGP allow Z to implement this policy?​
Yes, BGP enables Z to enforce the specified policy.

The network encompasses Autonomous Systems AS X, AS Y, and AS Z.
BGP stands for Border Gateway Protocol, serving as an Inter-AS routing protocol.
It acquires subnet reachability data from adjacent AS.
AS X has a peering agreement with AS Y.
AS Y has a peering agreement with AS Z.
This protocol empowers AS Z to formulate the policy.
Each AS maintains BGP route databases.
AS Y must indicate to AS X that it lacks a path to Z.
AS X remains unaware of AS Y's connection to AS Z.
AS X refrains from forwarding the traffic.
AS Z can route all of Y's traffic.
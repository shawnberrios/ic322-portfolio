# Wireshark Lab: Getting Started v8.1

## Introduction

The purpose of this lab was to manually follow the tracing of certain packets via different applications. In this case, we only tracked one HTTP file through Wireshark and we were able to see the messages being sent and received from protocols at different levels of the protocol stack.

## Process

I simply followed the instructions entirely from the listed URL address, [click me!](http://gaia.cs.umass.edu/kurose_ross/wireshark.php) (version 8.1).

#### 1. Which of the following protocols are shown as appearing (i.e., are listed in the Wireshark "protocol" column) in your trace file: TCP, QUIC, HTTP, DNS, UDP, TLSv1.2?

After opening up the trace file for my Wireshark PcapNg (PCAP Next Generation Dump File Format), I was able to see all sorts of protocols. The ones that I saw in my trace file were `TCP, HTTP, DNS, UDP, and TLSv1.2`. I did not see any QUIC protocols.

#### 2. How long did it take from when the HTTP GET message was sent until the HTTP OK reply was received? (By default, the value of the Time column in the packet-listing window is the amount of time, in seconds, since Wireshark tracing began. If you want to display the Time field in time-of-day format, select the Wireshark View pull down menu, then select Time Display Format, then select Time-of-day.)

It took about 0.0235 seconds, which is roughly `23.5ms`:

![Q2](week/1/get-req-time.PNG)

#### 3. What is the Internet address of the gaia.cs.umass.edu (also known as www-net.cs.umass.edu)?  What is the Internet address of your computer or (if you are using the trace file) the computer that sent the HTTP GET message?

The internet address of gaia.cs.umass.edu can be found at `128.119.245.12`, while mine is `10.25.134.191`.

#### 4. Expand the information on the HTTP message in the Wireshark “Details of selected packet” window (see Figure 3 above) so you can see the fields in the HTTP GET request message. What type of Web browser issued the HTTP request?  The answer is shown at the right end of the information following the “User-Agent:” field in the expanded HTTP message display. [This field value in the HTTP message is how a web server learns what type of browser you are using.]

My User Agent displays:
```
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36\r\n
```

Chrome or Safari or Mozilla? Hmm. `Chrome` is the best application for web browsing, so that is the one I use. Additionally, the reason why there are multiple Web browsers is because (1), most Web browsers use that same User-Agent string value when sending HTTP GET request messages ([StackExchange](https://security.stackexchange.com/questions/126407/why-does-chrome-send-four-browsers-in-the-user-agent-header)) and (2), some browsers restrict access to particular people depending on the browser that initiated the GET request ([StackOverflow](https://stackoverflow.com/questions/4024230/strange-user-agent-with-google-chrome)). In order to fix this, the browser masks your identity because the application you are using should not need to know where you are sending request messages.

#### 5. Expand the information on the Transmission Control Protocol for this packet in the Wireshark “Details of selected packet” window (see Figure 3 in the lab writeup) so you can see the fields in the TCP segment carrying the HTTP message. What is the destination port number (the number following “Dest Port:” for the TCP segment containing the HTTP request) to which this HTTP request is being sent?

The destination port address is `80`. AKA, the HTTP port.

#### 6. Print the two HTTP messages (GET and OK) referred to in question 2 above. To do so, select Print from the Wireshark File command menu, and select the “Selected Packet Only” and “Print as displayed” radial buttons, and then click OK.

I was unable to copy contents from the PDF and upload them here, so I have provided a link to view them below.

[click here to see my HTTP messages!](week/1/wireshark-get-print.pdf)
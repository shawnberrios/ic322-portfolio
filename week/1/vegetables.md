# Kurose, Chapter 1 Questions

#### R1. What is the difference between a host and an end system? List several different types of end systems. Is a Web server an end system?
The difference between a host and an end system is... well, there is none. End systems are also considered hosts because they both do the same thing, i.e., run applications. There is an ever-so-increasing rate of the different types of end systems in the modern age. Traditionally, end systems only consisted of the typical desktop computer (e.g., desktop PCs, Macs, and Linux boxes), servers (e.g., Web and e-mail servers), and mobile devices (e.g., laptops, smartphones, and tablets). But nowadays, end systems have expanded to include "things" such as game consoles, surveillance systems, watches, eye glasses, thermostats, and cars. Web servers are considered a type of end system.

#### R4. List four access technologies. Classify each one as home access, enterprise access, or wide-area wireless access.
* **Digital Subscriber Line (DSL)** - Home access
* **Cable Internet** - Home access
* **Mobile Internet** - Wide-area wireless access
* **Local Area Network** (LAN) - Enterprise (and home) access

#### R11. Suppose there is exactly one packet switch between a sending host and a receiving host. The transmission rates between the sending host and the switch and between the switch and the receiving host are R1 and R2, respectively. Assuming the switch uses store-and-forward packet switching, what is the total end-to-end delay to send a packet of length L? (Ignore queuing, propagation delay, and processing delay.)

It will take approximately **L/R1 + L/R2**.

![R11](week/1/eatvegpic.PNG)

#### R12. What advantage does a circuit-switched network have over a packet-switched network? What advantages does TDM have over FDM in a circuit-switched network?

Mainly, in circuit-switched networks, the resources that are required, e.g. buffers and link trasmission rate, are *reserved* for the duration of the communication process among the end systems. In packet-switched networks, these resources are not reserved, in which a session's messages use only the resources on demand and, as a consequence, may have to wait longer to access the communication link. Time-division multiplexing (TDM) is better than frequency-division multiplexing (FDM) in a circuit-switched network because it is just easier to implement. With an FDM configuration, the network must be partitioned among certain frequencies. Where as in TDM, the time is the only thing that must be handled, since frames are the items being divided in a fixed duration.

#### R13. Suppose users share a 2 Mbps link. Also suppose each user transmits continuously at 1 Mbps when trasmitting, but each user tranmits only 20 percent of the time. (See the discussion of statistical multiplexing in Section 1.3.)

    a. When circuit switching is used, how many users can be supported?
    
    When circuit switching is used, if 'x' users are sharing a 2 Mbps link with each user
    transmitting at 1 Mbps, that means there are 2Mbps/1Mbps = 2 users.

    b. For the remainder of this problem, suppose packet switching is used. Why will there be essentially
    no queuing delay before the link if two or fewer users transmit at the same time? Why will there be a
    queuing delay if there users transmit at the same time?
    
    Essentially, no queuing delay will ever happen since they are only able to transmit continuously at 1Mbps.
    There packets are not big enough to exceed the bandwidth, which is 2Mbps. If somehow they were able to exceed
    the incoming packets at once to 3Mbps, then there could be a queue.
    
    c. Find the probability that a given user is transmitting.
    
    The probability of all users transmitting simultaneously is given by the formula: probability = percentage^(amount of users).
    So in this case, 0.2^2=0.04, which is 4%.

    d. Suppose now there are three users. Find the probability that at any given time, all three users
    are transmitting simultaneously. Find the fraction of time during which the queue grows.
    
    The probability that all three users are trasmitting at the same time is 0.008, which is 0.8%. The queue will grow 1/125 of the time.

#### R14. Why will two ISPs at the same level of the hierarchy often peer with each other? How does an IXP earn money?

ISPs at the same level will often peer with eachother because "To reduce these costs, a pair of nearby ISPs at the same level of the hierarchy can **peer**, that is, they can directly connect their networks together so that all the traffic between them passes over the direct connection rather than through upstream intermediaries." Often times, these same ISPs will often set up settle-ment free transactions, so that neither ISP has to pay the other. Additionally, there are third-party companies that create Internet Exchange Point (IXP), in which multiple ISPs can join and peer together. IXPs earn their money by "selling their services based on port..." [PacketPushers](https://packetpushers.net/htirw-internet-exchange-points/#:~:text=IXPs%20sell%20their%20services%20based,it's%20the%20same%20basic%20concept.)

#### R18. How long does it take a packet of length 1,000 bytes to propagate over a link of distance 2,500 km, propagation speed 2.5*10^8 m/s, and transmission rate 2 Mbps? More generally, how long does it take a packet of length L to propagate over a link of distance d, propagation speed s, and transmission rate R bps? Does this delay depend on packet length? Does this delay depend on transmission rate?

The delay of a given packet will always depend on queuing, transmission, and propagation issues, and it is the combination of these three that equal the total nodal delay of that packet. **Queuing delay** is referred to as the delay that a packet experiences as it waits to be transmitted onto the link. This delay depends on the number of earlier-arriving packets that are queued and waiting for the transmission onto the link. Since packets are not processed immediately, they are processed in a queue, which is referred to as the "buffer." There is no formula for this as it all depends. Three factors that influence the queuing delay are if it is empty or not, if the packets arrive in small bursts, and the amount of open servers. The larger the queue, the longer it will take for the packet to be processed because all the other packets before it need to be processed as well. If the queue is empty, there will essentially be no delay. If large packets arrive in small bursts, it will be hard for the queue to process all the data quickly; imagine throwing a bunch of late homework on your professor's office table. And lastly, the more servers available, the more routes a packet can take to eventually end up at its final destination [GeeksForGeeks](https://www.geeksforgeeks.org/delays-in-computer-network/). The **transmission delay** is described as the delay that a packet-switched network undergoes as all the packet's bits are pushed into a wire. This delay is dependent on the size of data and the channel bandwidth of the wire. Transmission delay is a reciprocate of packet length and is characterized by the function *D = N/R*, where 'D' is the transmission delay in seconds (what we are trying to find), 'N' is the number of bits, and 'R' is the rate of transmission [Wiki](https://en.wikipedia.org/wiki/Transmission_delay). Since most networks utilize a store-and-forward transmission to pass data, it introduces an additional store-and-forward transmission delay. **Propagation delay** is described as the time required to propagate from the beginning of the link to router B. Packet delay *does* depend on the packet length. It will take *(2500000 m/2.5 x 10^8 m/s)+(1000 bytes/2000000 bytes) = 0.0105 seconds*.

#### R19. Suppose Host A wants to send a large file to Host B. The path from Host A to Host B has three links, of rates R1 = 500 kbps, R2 = 2 Mbps, and R3 = 1 Mbps.

    a. Assuming no other traffic in the network, what is the throughput for the file transfer?
    
    The total throughput for the file transfer is 500 kbps, since that is the most the first link can process.

    b. Suppose the file is 4 million bytes. Dividing the file size by the throughput, roughly how long will it take to transfer the file to Host B? 
    
    Since kbps is *kilobytes per second* and not *kibibytes*, you must convert 500 kbps into SI units, which is 500000 bytes.
    You then get, 4*10^6 bytes/ 500*10^3= 8 seconds.

    c. Repeat (a) and (b), but now with R2 reduced to 100 kbps.
    
    Since the throughput is reduced to 100 kbps, the answer to 'b' changes to 40 seconds.
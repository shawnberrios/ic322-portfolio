# Week 1: The Internet

This week introduces the topic of the Internet through concepts of packets, delays, and encapsulation.

* [Main](wireshark.md)
* [I ate my vegetables](week/1/vegetables.md)
* [Learning Objectives](week/1/learning-objectives.md)
# Kurose, Chapter 8 Questions

#### R20. In the TLS record, there is a field for TLS sequence numbers. True or false?
The SSL record includes a type field, version field, length field, data field, and MAC field, but it does not contain a field for SSL sequence numbers.

#### R21. What is the purpose of the random nonces in the TLS handshake?
Preventing a replay attack relies on the server's refusal to accept duplicate nonces. If someone attempts to resend a captured message, it gets denied because the nonce, once recorded, cannot be reused.

#### R22. Suppose an SSL session employs a block cipher with CBC. True or false: The server sends to the client the IV in the clear.
Incorrect. In an SSL (Secure Sockets Layer) session using a block cipher with CBC (Cipher Block Chaining), the server cannot transmit the IVs (Initialization Vectors) in plaintext because both the server and client can generate IVs independently.

#### R23. Suppose Bob initiates a TCP connection to Trudy who is pretending to be Alice. During the handshake, Trudy sends Bob Alice’s certificate. In what step of the TLS handshake algorithm will Bob discover that he is not communicating with Alice?
During step 6 of the SSL handshake algorithm, it becomes evident that Bob isn't communicating with Alice. This realization stems from the following sequence of events: Bob initiates a TCP connection with Trudy, who impersonates Alice. Trudy sends Bob Alice's certificate in step 2, which Bob verifies as belonging to Alice in step 3. In step 4, a Master Secret (MS) is formed from PMS and nonces from both sender and receiver. Subsequently, Bob sends messages along with their MAC in step 5. Finally, in step 6, Trudy sends the MAC of messages, but they fail to pass the MAC test. Consequently, Bob discerns that the communication isn't with Alice.

#### P9. In this problem, we explore the Diffie-Hellman (DH) public-key encryption algorithm, which allows two entities to agree on a shared key. The DH algorithm makes use of a large prime number p and another large number g less than p. Both p and g are made public (so that an attacker would know them). In DH, Alice and Bob each independently choose secret keys, SA and SB, respectively. Alice then computes her public key, TA, by raising g to SA and then taking mod p. Bob similarly computes his own public key TB by raising to SB and then taking mod p. Alice and Bob then exchange their public keys over the Internet. Alice then calculates the shared secret key S by raising TB to SA and then taking mod p. Similarly, Bob calculates the shared key S′ by raising TA to SB and then taking mod p.
##### a. Prove that, in general, Alice and Bob obtain the same symmetric key, that is, prove S = S′.
![a](week/15/images/a.PNG)
##### b. With p = 11 and g = 2, suppose Alice and Bob choose private keys SA = 5 and SB = 12, respectively. Calculate Alice’s and Bob’s public keys, TA and TB. Show all work.
![b](week/15/images/b.PNG)
##### c. Following up on part (b), now calculate S as the shared symmetric key. Show all work.
![c](week/15/images/c.PNG)
##### d. Provide a timing diagram that shows how Diffie-Hellman can be attacked by a man-in-the-middle. The timing diagram should have three vertical lines, one for Alice, one for Bob, and one for the attacker Trudy.
![d](week/15/images/d.PNG)

#### P14. The OSPF routing protocol uses a MAC rather than digital signatures to provide message integrity. Why do you think a MAC was chosen over digital signatures?
A notable aspect of a MAC is its independence from an encryption algorithm (p692). MACs find application in prevalent security protocols at the transport and network layers (p697). Entities focused solely on message integrity, not confidentiality, benefit from MAC usage. Through MACs, these entities authenticate messages without needing intricate encryption algorithms within the integrity process (p692). Digital signatures, on the other hand, rely on a foundation like Public Key Infrastructure and frequently necessitate certification authorities.

#### P23. Consider the example in Figure 8.28. Suppose Trudy is a women-in-the-middle, who can insert datagrams into the stream of datagrams going from R1 and R2. As part of a replay attack, Trudy sends a duplicate copy on one of the datagrams sent from R1 to R2. Will R2 decrypt the duplicate datagram and forward it into the branch-office network? If not, describe in detail how R2 detects the duplicate datagram.
Consider this scenario:

Trudy acts as a middlewoman intercepting datagrams between R1 and R2. Within this stream, she inserts a datagram and duplicates another for a replay attack.

In this setup, R2 refrains from decrypting the inserted datagram and doesn't pass it along to the branch office network due to specific reasons:

First check: R2 verifies the sequence number of the datagram packet received. Decryption occurs only if the ESP header and sequence number counter value align. If they don’t match, the packet gets discarded.
Second check: R2 examines the subfield of the sequence number in the ESP field header, comparing it with the sequence number subfields of already received datagram packets. Any match results in discarding the datagram packet.

Thus, R2 dismisses the duplicated packet sent by Trudy.
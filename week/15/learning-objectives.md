# Learning Objectives

#### 1. I can explain how two strangers are able to exchange secret keys in a public medium.
The fundamental principle behind key exchange involves the utilization of a public-private key mechanism. An essential characteristic of this system is the distinct roles of the public and private keys: a message encrypted with a public key necessitates the corresponding private key for decryption, and vice versa. Furthermore, the private key cannot be derived from its associated public key. This mechanism, often implemented through specific modular arithmetic, aligns with these specified traits. A specific example of such key exchange is the Diffie-Hellman.

However, comprehending the basic notion of how secret keys can be shared across a public channel doesn't demand an in-depth understanding of the algorithms involved. Consider two entities, Alice and Bob, aiming to exchange a secret key discreetly. Let's introduce another party, Eve, who has access to all information transmitted over this channel. Alice and Bob opt to use a public-private key system: they distribute their public keys publicly while safeguarding their private keys.

When Alice intends to transmit the secret to Bob, she encrypts the message using Bob's public key and transmits it. Bob, upon receiving this encrypted message, decrypts it using his private key. Remembering the principle that a message encrypted with a public key requires the corresponding private key for decryption, Bob exclusively possesses the capability to decipher the message. Despite Eve intercepting the encrypted message, armed solely with Bob's public key, she remains unable to decrypt it. The inherent property of the private key's secrecy, not derivable from the public key, reinforces this security measure. Consequently, Bob comprehends Alice's message while Eve remains unaware of its content.

For Bob to send a message to Alice, the process mirrors the previous steps, with Bob utilizing Alice's public key for encryption, and Alice employing her private key for decryption.

Diffie-Hellman's key exchange has the following steps:

1. Public Parameters
2. Private Calculation
3. Public Calculation
4. Exchange Public Values
5. Key Calculation

#### 2. I can walk through the TLS handshake and explain why each step is necessary.
1. Alice, in her role as the client, transmits a plaintext catalog of cryptographic algorithms supported. This step aims to synchronize both Alice and Bob, ensuring mutual agreement on the algorithm selection.
2. Subsequently, Bob, functioning as the server, makes selections: a symmetric algorithm, a public key algorithm, and an HMAC algorithm along with their respective HMAC keys. Additionally, Bob dispatches a certificate and a server nonce.
3. Alice undertakes certificate verification, validating Bob's public key embedded within the certificate. In parallel, she generates a pre-master secret (PMS), encrypts it using Bob's public key, and forwards this encrypted PMS.
4. Leveraging the pre-established public key algorithm, Bob decrypts the encrypted pre-master secret, deriving the Master Secret (MS), and generates two encryption keys, two HMAC keys, and potentially initialization vectors.
5. To ensure data integrity, Alice transmits an HMAC of all previously exchanged messages, fortifying the defense against message tampering by external entities.
6. In parallel, Bob mirrors this action, sending an HMAC of all exchanged messages for analogous security reasons.

The book mentions the steps specifically and they are listed below:

1. "The client sends a list of cryptographic algorithms it supports" and a nonce.
2. The server selects a symmetric algorithm, a public key algorithm, an HMAC algorithm, and corresponding HMAC keys. Subsequently, it transmits these selections to the client along with a unique nonce.
3. "The client verifies the certificate, extracts the server's public key, and generates a Pre-Master Secret" Encrypting the PMS with the public key and then sending the encryption to the server.
4. Both the client and server independently compute the Master Secret using the Pre-Master Secret (PMS) and nonces. This Master Secret is then segmented to derive encryption and HMAC keys.
5. "The client sends the HMAC of all the handshake messages."
6. "The server sends the HMAC of all the handshake messages."

#### 3. I can explain how TLS prevents man-in-the-middle attacks.
TLS thwarts man-in-the-middle attacks by employing specific measures within its handshake and session termination processes. These safeguards extend throughout the connection, bolstered by robust encryption algorithms. The handshake incorporates diverse strategies to thwart such attacks. Both parties introduce fresh nonces, preventing any misuse of transmitted data to impersonate either the server or the client. Moreover, TLS bars data tampering by transmitting HMACs of the exchanged messages, verifying their integrity. Mismatched HMACs lead to connection termination.

Further fortification against premature handshake termination, a potential avenue for attacks, is established. Terminating the TCP connection alone would be insufficient, as an attacker could exploit this by sending a TCP FIN signal. To counter this, either the server or client must signal their intent to end the TLS session via the type field. Any unindicated session termination would raise an alert, signaling potential foul play to the client or server.
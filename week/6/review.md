# Week 6: Six Week Review

During the first six weeks of this class we laid the foundations of 

- How the Internet works, how ISPs connect together, and how our computers and systems connect to the Internet
- How applications talk to each other using HTTP.
- We learned how to query API endpoints in order to run programs on other computers and receive their output.
- We learned what domain names are and how our computers use them to address our data.
- We know how email works! To a degree. But we do know how it's different than webpages (HTTP). We might even use email's example to build our own custom protocols.
- We now know what the Transport Layer does and how TCP creates a reliable data channel in a lonely, unreliable world.


## Week 1: The Internet

1. Open the feedback I provided for you for Week 1. Read the feedback.
2. Open your Portfolio to your Week 1 Learning Objectives. Read your answers.
3. Open your Portfolio to your Week 1 Chapter Questions. Review your answers. Do you think you put more effort into these answers than in later weeks? Less? About the same? Why do you think that is?
I would say that the effort I have put into later weeks of this class have been about the same. I chose a format that works for me at the beginning and that has easily supplemented how I manage my time with this class. I will say that I have added more pictures and visual aids that help me out, especially when it comes to knowledge checks.
4. Review your Week 1 Lab. How have your Markdown skills evolved over the past 6 weeks?
My markdown skills have been fairly consistent in encorporating all sorts of formates throughout the past 6 weeks.
5. Which classmate's portfolio were you paired with for this assignment?
[Lauren Leckelt!](https://gitlab.com/lleckelt1/ic322-profile)
6. Review your classmate's Week 1 submission. What is the same about their Assignment solution as compared to yours? What is different?
Lauren's portfolio for Week 1 was amazing. It followed a very structured format and all her answers were easy to read. She even added many markdown formats that demonstrated her proficiency in using markdown. There was nothing different about hers compared to mine, as we both even included a `README.md` with links to all the assignments.
7. Open an issue and leave some feedback for your classmate. Try to mention at least one thing you like, and one thing you think could be improved (and some encouraging words to send them on their way down the path of improvement.)
8. Between your Portfolio and your classmate's, is one Portfolio "better"? Or is the answer more complicated than that?
Of course, I'd like to say that mine is better, but the truth is, both convey a purpose that is essential to one's own understanding. I cannot judge if one is better because it has "more pictures" because if she doesn't learn well with pictures, then it makes sense why there aren't any there.

## Week 2: The Application Layer, HTTP

1. Open your Week 2 classmate's Portfolio. Did they make use of Markdown in any creative ways? Keep their Portfolio open.
I do not think that Anthony used any special markdown techniques for Week 2.

2. Open the feedback I provided for Week 2. Read the feedback.

3. Open your Week 2 lab.
    - If you chose one of the programming assignments, review the code. Do you still remember what each line does? Does your "code style" make you happy?
    - If you chose the Wireshark lab, read your answers. If someone read your lab report but did not take IC322, would they learn anything?
    I believe if someone were to read my Wireshark lab, that they would learn something. In my lab, I provided ample visual aids that correspond to the message/answer I was trying to convey. I also provided numerous markdown formats that allows easier understanding of the material.
3. Read your Week 2 Review Questions and Learning Goal answers. Did you do a better or worse job during Week 2 compared to Week 1?
I did about the same compared to Week 1.
4. Go back to your classmate's Portfolio.
5. Which lab did they choose?
    - If it was a programming lab, try to run their program.
    - If it was a Wireshark Lab, try to learn something new from their answers. Include the new thing you learned when you leave them feedback.
    Anthony's answer to question 9 piked my interest, "Inspect the contents of the server response. Did the server explicitly return the
contents of the file? How can you tell? The server explicitly returned the contents of the file in the Line-based text data, which stated that the file if downloaded multiple times will only be sent once by the server due to the inclusion of the IN-MODIFIED-SINCE field." I did not kow that the file would only be sent once by the server.
6. Look at their Review Questions and Learning Goal answers. Try to learn something new. Include the new thing you learned when you leave them feedback.
After reading Anthony's R12, I was finally able to understand how cookies work whenever a user visits the same site multiple times, "The web browser will append a line to the special cookie file it manages and each time the customer visits the page, the browser will go to the cookie file and extract the identification number and puts the identfication number in the HTTP request." I was unclear on this as I did not even answer it in my own portfolio.
7. Open an issue and leave feedback for your classmate. Try to mention at least one thing you like, and one thing you think could be improved. Try to say something that could cheer up someone having a bad day.
8. If you were to give your classmate a grade for their Week 2 submission, what would it be? (They won't see your answer. This is an exercise for *you*.)
I would give Anthony an 'A'. He used additional resources to supplement his answers.

## Week 3: The Application Layer, DNS and Email

1. Open your Week 3 Portfolio. Read your Learning Goal answers and your answers to the Review Questions. Without looking at my feedback, can you remember what feedback I gave you? Do you have feedback for yourself?
If I recall correctly, I think some of my answers for both the learning goals and review questions were wrong. I would say just make sure you are answering the *right* question.
2. Open your Week 3 partners's Portfolio. Which Lab did they do? If it was the DNS lab, visit their website.
My partner did the Wireshark Lab.
3. Read your classmate's Learning Goal and Review Question answers. What is one thing that they do that you would like to do in future submissions?
When including block quotes, Ryan used a '>', which is a markdown technique that separates that quote from the rest of the paragraph.
4. Open an issue and leave feedback for your classmate. Try to mention at least one your classmate did in their Portfolio that you like, and one practice they could incorporate in the future.

## Week 4: Transport Layer

1. Read the feedback I provided for you for Week 4. Have there been any patterns so far in the feedback I've left for you?
The only pattern that I could point out was that you have enjoyed the amount of work I have put into my portfolio. Thanks!
2. Open your Learning Goal answers. Have you made any mistakes that were addressed in previous weeks' feedback?
No!
3. Open your Week 4 partner's Portfolio. Read their Learning Goal Answers. Did they make any mistakes?
No! Their answers are extremely well-written and demonstrate a more-than-capable understanding of the material.
4. Open an issue and leave feedback for your classmate.
5. Read your Review Question Answers.
6. Review your lab report.
7. What was the most important concept we learned this week?
To introduce and explore TCP through concepts like sequence numbers, ACKs, control mechanisms, and other congestion algorithms.

## Week 5: Transport Layer, Part 2

1. Open your Week 5 assignment submission in your Portfolio.
2. Read all your written work.
3. Open your Week 5 partner's portfolio. Read their written work.
4. What about your solutions do you like better than your partner's?
My solutions to this week were not the best, but I do like how I separated the two: learning objectives and review questions.
5. What about your partner's solutions do you like better than yours?
Forrest's Week 5 is more extensive and is bulleted, making it easier to read.
6. Open an issue and leave feedback for your classmate.

## Knowledge Checks


## General Reflection

1. Some people make a distinction between "learning" and "answering". Do you think there's a difference? How much of the first six weeks did you spend "learning", and how much time did you spend "answering"?
In complete honesty, this class has been a little difficult to adjust to. As you have brought up, I think most of this class has been answering questions, rather than learning. I do think there is a difference and I hypothesize the reason "learning" has been so difficult is just because of the amount of work we have to do. If we had less questions and more time to digest what we are reading, I think that would benefit not only me, but others, as well.

2. At the beginning of the semester I said I had 3 goals for this class, in order of importance: 1. For you to learn Computer Networks, 2. for you to enjoy the class, and 3. for you to "learn how to learn". How successful have I been so far?
I think you have done a good job at guiding us to find different avenues to learn material, but I definitely feel that this class is too heavy for our own personal journey in regards to Computer Networks.

3. Every week I've asked you to "Do at least one thing to improve the physical or digital space of the class." Try to remember, every week, did you do something to improve the community? Try to make a mental list.
It has been difficult to find physical things to improve, but there is the portion of the class on `quizziz` that involves improving exisiting questions. Sometimes if I can't think of anything more to do in the classroom, I will just contribute extra on that website.
4. You didn't spend enough time thinking about #3, so let's think some more. Think to yourself: what will I do next week to make my classmates' experience better?
I can offer help to others if they don't understand something as well, or I can offer help on how to have a killer portfolio.

## Feedback

1. I've been lecturing a little bit every day, including "lab days". I aim to lecture for 1 to 1.5 hours total during a week. Has this been working for you? Do you want more/less lecture?
I think that our interactions where we come up with problems and rotate to solve another one have definitely aided in my understanding of the material. 
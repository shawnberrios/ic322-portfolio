# Learning Objectives

#### LO1. I can explain the ALOHA and CSMA/CD protocols and how they solve the multiple-access problem.
The multiple access problem in computer networking refers to the challenge of efficiently and fairly sharing a communication medium among multiple network devices that are attempting to transmit data. In a network, multiple devices may need to send or receive data simultaneously, and managing how these devices access the shared communication channel is a critical aspect of network design.

There are several methods to address the multiple access problem, and different network architectures use different approaches. But I will only describe ALOHA and CSMA/CD:

**Carrier Sense Multiple Access (CSMA)**: CSMA is a protocol used in Ethernet networks, where devices listen to the communication channel before transmitting. If the channel is busy, they wait for a random amount of time before attempting to transmit again.

    **CSMA/CD (Collision Detection)**: In traditional Ethernet networks, if two devices attempt to transmit simultaneously and a collision occurs, they detect the collision and wait for a random backoff time before retransmitting.

    **CSMA/CA (Collision Avoidance)**: Used in wireless networks, CSMA/CA involves devices informing others of their intention to transmit, reducing the chances of collisions.

**ALOHA**: ALOHA allows network devices to transmit data whenever they have something to send. The shared communication channel is treated as a single, continuous stream. 
ALOHA follows a procedural process, which begins with transmission:
When a device wants to transmit data, it sends the data onto the communication channel. The transmission time is not synchronized with other devices.

Collision Detection:
After transmitting a frame, the sender waits for an acknowledgment. If an acknowledgment is not received within a specified time (acknowledgment window), the sender assumes that the frame collided with another transmission.

Collision Resolution:
In case of a collision, the sender waits for a random amount of time and then retransmits the frame.

Types of ALOHA:

    **Pure ALOHA**: Devices transmit whenever they have data and then listen for acknowledgments. Collisions are detected through the absence of acknowledgments.
    **Slotted ALOHA**: Time is divided into slots, and devices can only transmit at the beginning of a slot. This reduces the chances of collisions and simplifies collision detection.

Efficiency:
ALOHA is not highly efficient, especially under heavy network loads, as collisions increase with more devices attempting to transmit.
Slotted ALOHA improves efficiency by reducing the likelihood of collisions, but it still faces challenges in high-traffic scenarios.

#### LO2. I can compare and contrast various error correction and detection schemes, including parity bits, 2D parity, the Internet Checksum, and CRC.

**Single Bit Parity** - In single bit parity, a single additional bit is added to a data stream to make the total number of set bits (1s) either even (even parity) or odd (odd parity). The receiver checks this parity bit to determine if the number of set bits in the received data is consistent with the agreed-upon parity (even or odd). If the parity doesn't match, an error is detected. Detects errors involving an odd number of bit flips.

**2D parity** - In contrast, 2D parity extends the concept to a two-dimensional array or matrix. The data is arranged in rows and columns, and two parity bits are calculated: one for the rows (row parity) and one for the columns (column parity). The parity bits are then sent along with the data. At the receiver, both row and column parities are recalculated, and any inconsistencies indicate the presence of errors. Detects errors not only in individual bits but also errors that affect entire rows or columns. Can identify and locate errors in both horizontal and vertical directions. Improves error detection capability compared to single bit parity.

The biggest difference among the two is that: single bit parity is simpler and involves only one parity bit, 2D parity is more complex due to the calculation of both row and column parities, single bit parity can detect odd numbers of bit flips but is limited in its error detection capabilities,
2D parity can detect a broader range of errors, including both odd and even numbers of bit flips.

**Internet Checksum** - The Internet checksum is determined by dividing the data into segments of size 'k' and then summing them. The 1's complement of this sum is computed and incorporated into the segment header. Upon receiving the packet, the receiver verifies the checksum by calculating the 1's complement of the data, including the checksum. A sum of 0 indicates no errors, while a sum of 1 signifies the presence of an error. Checksums are commonly applied at the transport layer due to their simplicity and speed, offering a quick means of error detection.

Using no parity bit, there is no error detection; using a single bit of checksum (a parity) we [can potentially] detect 50% of the errors. 

**CRC** - Also known as Cyclic Redundancy Check, CRC is another way to detect errors for the link layer. CRC is a method for error detection in digital data transmission. It involves representing data as a polynomial and appending a CRC remainder to create a longer bit sequence. The sender and receiver use a predetermined polynomial for division. If the remainder obtained during division is zero, no errors are detected. If it's nonzero, errors are present. CRC is effective for detecting errors, especially burst errors, but it doesn't correct them. Instead, it often triggers a request for retransmission.

#### LO3. I can describe the Ethernet protocol including how it implements each of the Layer 2 services and how different versions of Ethernet differ.
Ethernet, a prevalent wired Local Area Network (LAN) technology, is widely utilized globally. LANs function in the Link Layer, utilizing link-layer frames transmitted through switches (link layer packet switches). Unlike the Network Layer, which uses IP addresses, Ethernet employs MAC addresses, ensuring each address is unique, preventing duplication among adapters.

Initially, Ethernet employed a hub-based topology, where a physical layer device, the hub, forwarded every received bit from one interface to the others. However, modern Ethernet has shifted to using switches that operate at the link layer, employing store-and-forward packet switching. Ethernet offers a connectionless service to the network layer by encapsulating a layer 3 datagram into a layer 2 frame, transmitting it across the LAN without requiring confirmation from the destination, akin to UDP's connectionless service at layer 4.

Distinct Ethernet versions are distinguished by acronyms like 10GBASE-T, with the initial numbers indicating the speed, in this case, 10 Gigabits per second. "BASE" denotes exclusive Ethernet traffic on the medium, and the last part specifies the physical media type; for instance, "T" signifies twisted-pair copper wire.
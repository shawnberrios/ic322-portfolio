# Week 12: Link Layer

This week talks about the link layer!

* [Main](week/12/main.md)
* [Partner Feedback](https://gitlab.com/calebwalker/ic322-portfolio/-/issues/21)
* [I ate my vegetables](week/12/vegetables.md)
* [Learning Objectives](week/12/learning-objectives.md)
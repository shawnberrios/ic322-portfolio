# Kurose, Chapter 6 Questions

#### R4. Suppose two nodes start to transmit at the same time a packet of length L over a broadcast channel of rate R. Denote the propagation delay between the two nodes as dprop. Will there be a collision if dprop < L/R? Why or why not?
The propogation delay is L/R, which describes the time needed for one node to propagate a single packet. There will be a collision if dprop is less than the L/R. This is because the first packet is not fully propagated across the channel yet. The channel will be idle by the time transmission ends and the next propagation phase begins.

#### R5. In Section 6.3, we listed four desirable characteristics of a broadcast channel. Which of these characteristics does slotted ALOHA have? Which of these characteristics does token passing have?
**Single-Channel**: Both slotted ALOHA and token passing have this characteristic. In slotted ALOHA, there is a single channel through which all nodes communicate. Similarly, in token passing, there is a single token circulating through the network, and only the node holding the token can transmit.

**No Coordination**: Slotted ALOHA does not require coordination among the nodes during the transmission phase, as each node simply waits for its assigned time slot. Token passing, on the other hand, requires coordination as nodes must follow the order of the token and can only transmit when they possess the token.

**Collision-Free**: Slotted ALOHA is collision-free within a slot. If two nodes attempt to transmit in the same slot, a collision occurs, and the data is corrupted. In token passing, collisions are avoided because only the node with the token is allowed to transmit.

**Simple**: Slotted ALOHA is relatively simple, as nodes only need to contend for a time slot. Token passing is more complex as it involves the circulation of a token and the need for nodes to follow a protocol for accessing the channel.

#### R6. In CSMA/CD, after the fifth collision, what is the probability that a node chooses K=4? The result K=4 corresponds to a delay of how many seconds on a 10 Mbps Ethernet?
After the fifth collision, the probability that a node chooses K=4 is 1/32. You get this by dividing 1/2^4. The result K=4 corresponds to a delay of 0.03125 seconds. You get this by doing K*512/2^20.

#### P1. Suppose the information content of a packet is the bit pattern 1110 0110 1001 0101 and an even parity scheme is being used. What would the value of the field containing the parity bits be for the case of a two-dimensional parity scheme? Your answer should be such that a minimum-length checksum field is used.
![p1](week/12/images/p1.PNG)

#### P3. Suppose the information portion of a packet (D in Figure 6.3) contains 10 bytes consisting of the 8-bit unsigned binary ASCII representation of string “Internet.” Compute the Internet checksum for this data.
The internet checksum for this data is `1111001111100000`. After adding the 16-bit integers you get `0000110000011111`, and then taking the 1's compliment, you get the internet checksum.

#### P6. Consider the previous problem, but suppose that D has the value:
##### a. 1000100101
![p6a](week/12/images/p6a.PNG)

##### b. 0101101010
![p6b](week/12/images/p6b.PNG)

##### c. 0110100011
![p6c](week/12/images/p6c.PNG)

#### P11. Suppose four active nodes—nodes A, B, C and D—are competing for access to a channel using slotted ALOHA. Assume each node has an infinite number of packets to send. Each node attempts to transmit in each slot with probability p. The first slot is numbered slot 1, the second slot is numbered slot 2, and so on.
##### a. What is the probability that node A succeeds for the first time in slot 4?
The probability that node A succeeds for the first time in slot 4 is (1-p)^3* p *(1-p)^3.

##### b. What is the probability that some node (either A, B, C or D) succeeds in slot 5?
The probability that some node succeeds in slot 5 is p * 4 * 1/4.

##### c. What is the probability that the first success occurs in slot 4?
The probability that the first success occurs in slot 4 is 4*((1-p)^3)^4*p.

##### d. What is the efficiency of this four-node system?
The efficiency of this four-node system is 4p(1-p)^3.

#### P13. Consider a broadcast channel with N nodes and a transmission rate of R bps. Suppose the broadcast channel uses polling (with an additional polling node) for multiple access. Suppose the amount of time from when a node completes transmission until the subsequent node is permitted to transmit (that is, the polling delay) is d_poll Suppose that within a polling round, a given node is allowed to transmit at most Q bits. What is the maximum throughput of the broadcast channel?
The maximum throughput of the broadcast channel is described by 'data transmitted in one round/time to complete one round,' which is given by the formula Q/(R/Q)+d_{poll}.
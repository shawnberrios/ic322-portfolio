# Protocol Pioneer: Link Layer

## Introduction

The purpose of this lab is to find a way that'll increase the success rate of incoming packets, which will then be received at a server.

## Collab

Michael Choi, Nicholas Zayfman, and I worked together for this lab since there was an uneven amount of people during our class period.

## How did you solve the Chapters? Please copy and paste your winning strategy(s), and also explain it in English.

Michael introduced a great way in order to solve the level. He suggested we implement some sort of time division multiplexing that would manage which client would receive a particular packet. At first, the success rate was 0%, but with our implementation, it was stable around 0.1.

Here is a snippet of our strategy code:

```
from lib.Act02Chapter01 import Act02Chapter01
# Welcome to the StellarScript Console!

def server_strategy(self):
    
    self.set_display(f"Queued:{len(self.from_layer_3())}")

    # Handle messages received from our interfaces, destined for layer 3 
    while(self.message_queue):
            m = self.message_queue.pop()
            print(f"--- Server {self.id}: Msg on interface {m.interface}: Tick {self.world.get_current_tick()} ---\n{m.text}\n------------------")
            
            # If this message belongs to us, send it to layer 3
            if parse_message(m.text).get("Destination") == self.id:
                self.to_layer_3(m.text)

    # Time division multiplexing
    my_turn = False
    if self.id == "1" and (self.current_tick() % 40) < 10:
        my_turn = True
    elif self.id == "2" and (self.current_tick() % 40) < 20:
        my_turn = True
    elif self.id == "3" and (self.current_tick() % 40) < 30:
        my_turn = True
    elif self.id == "4" and (self.current_tick() % 40) < 40:
        my_turn = True

    if my_turn:
        self.set_display(f"My turn")
    else:
        self.set_display(f"waiting")


    # Handle messages from layer 3, destined for the interfaces
    # Each entity has a single interface in this scenario. Figure out what the interface is.
    connected_interfaces = self.connected_interfaces() # This should return a list of exactly one value.
    selected_interface = connected_interfaces[0] 
    if not self.interface_sending(selected_interface):
        if len(self.from_layer_3()) > 0:
            msg_text = self.from_layer_3().pop()
            print(f"{self.id}: Attempting send.")
            self.send_message(msg_text, selected_interface)

def client_strategy(self):

    self.set_display(f"Queued:{len(self.from_layer_3())}")

    # Handle messages received from our interfaces, destined for layer 3 
    while(self.message_queue):
            m = self.message_queue.pop()
            print(f"--- Client {self.id}: Msg on interface {m.interface}: Tick {self.world.get_current_tick()} ---\n{m.text}\n------------------")
            
            # If this message belongs to us, send it to layer 3
            if parse_message(m.text).get("Destination") == self.id:
                self.to_layer_3(m.text)

    # Time division multiplexing
    my_turn = False
    if self.id == "1" and (self.current_tick() % 40) < 10:
        my_turn = True
    elif self.id == "2" and (self.current_tick() % 40) < 20:
        my_turn = True
    elif self.id == "3" and (self.current_tick() % 40) < 30:
        my_turn = True
    elif self.id == "4" and (self.current_tick() % 40) < 40:
        my_turn = True

    if my_turn:
        self.set_display(f"My turn")
    else:
        self.set_display(f"waiting")

    # Handle messages from layer 3, destined for the interfaces
    # Each entity has a single interface in this scenario. Figure out what the interface is.
    connected_interfaces = self.connected_interfaces() # This should return a list of exactly one value.
    selected_interface = connected_interfaces[0] 
    if not self.interface_sending(selected_interface) and my_turn:
        if len(self.from_layer_3()) > 0:
            msg_text = self.from_layer_3().pop()
            print(f"{self.id}: Attempting send.")
            self.send_message(msg_text, selected_interface)
```

The idea was that if it was "your turn" (i.e., the incrementalization of the tick % some number) then the program would give priority to that client. We tried to play with the *some number*, but we found it best to be at 40.

## What was your maximum steady-state success rate (after 300 or so ticks?)
The maximum steady-state success rate was around 0.1.

## Evaluate your strategy. Is it good? Why or why not?

Given that the success rate increased and was a constant 0.1. I would say this was a fairly good strategy. There are definitely other ways to accomplish this packet-sending and receive process, but the TDM was fairly easy to implement.

## Are there any strategies you's like to implement, but you don't know how?

I am curious to see the best way to implement this program, and why the max success rate is around 0.2.
# Wireshark Lab: TCP v8.0

## Introduction

The purpose of this lab is to investigate the behavior of the celebrated TCP protocol in detail. More specifically, we will be analyzing a trace of TCP segments sent and received in the transmittance of a 150KB file from this computer to a remote server. The purpose is to understand the use of sequence and acknowledgement numbers for rdt; to look at TCP's congestion and control algorithm; and lastly, take a look at TCP's receiver-advertised flow control mechanism.

## Process

I simply followed the instructions entirely from the listed URL address, [click me!](http://www-net.cs.umass.edu/wireshark-labs/Wireshark_TCP_v8.0.pdf) (version 8.0). I could not use the latest version of the lab from the *Kurose* website (v8.1), so the v8.0 will suffice.

## 1. Capturing a bulk TCP transfer from your computer to a remote server

I was unable to run Wireshark on my live network connection, so I will have to follow using the packet trace file that was captured on one of the author's computers.

![Alice](week/4/uploadalice.PNG)
![No Packets Captured](week/4/nopacketcapture.PNG)

I will be following tcp-ethereal-trace-1 for this first part, which can be found [here](week/4/tcp-ethereal-trace-1).

## 2. A first look at the captured trace

### 1. What is the IP address and TCP port number used by the client computer (source) that is transferring the file to gaia.cs.umass.edu? To answer this question, it's probably easiest to select an HTTP message and explore the details of the TCP packet used to carry this HTTP message, using the "details of the selected packet header window" (refer to Figure 2 in the "Getting Started with Wireshark" Lab if you're uncertain about the Wireshark windows.)

After looking at the trace file, I was able to determine that the source IP originated from the address `192.168.1.102`. I chose that address because if you go to line 199, it annotates that the source IP address is `192.168.1.102`. In the question, it recommends to look at an HTTP message and explore the details of it to determine which TCP packet carried the HTTP message. Additionally, the TCP port number used by the client computer is `1161`.

![1-1](week/4/1-1.PNG)

### 2. What is the IP address of gaia.cs.umass.edu? On what port number is it sending and receiving TCP segments for this connection?

The IP address and TCP port number for gaia.cs.umass.edu is `128.119.245.12` and `80`, respectively.

![1-2](week/4/1-2.PNG)

### 3. If you have been able to create your own trace answer the following question: What is the IP address and TCP port number used by your client computer (source) to transfer the file to gaia.cs.umass.edu?

I am unable to create my own trace.

## 3. TCP Basics

### 4. What is the sequence number of the TCP SYN segment that is used to initiate the TCP connection between the client computer and gaia.cs.umass.edu? What is it in the segment that identifies the segment as a SYN segment?

The sequence number of the TCP SYN segment that is used to initiate the TCP connection between the client computer and gaia.cs.umass.edu is `0`. In the screen grab presented below, within the flags section, the Syn flag is set to 1, which indicates that the segment is a SYN segment.

![1-4](week/4/1-4.PNG)

### 5. What is the sequence number of the SYNACK segment sent by gaia.cs.umass.edu to the client computer in reply to the SYN? What is the value of the Acknowledgement field in the SYNACK segment? How did gaia.cs.umass.edu determine that value? What is it in the segment that identifies the segment as a SYNACK segment?

The sequence number of the SYNACK segment sent by gaia.cs.umass.edu to the client computer in reply to the SYN is 0. The value of the Acknowledgement field in the SYNACK segment is 1. This is determined by gaia.cs.umass.edu and becomes a 1 to the initial sequence number of the SYN segment from the client computer. Since the initial sequence number of the SYN segment from the client computer is 0, the value of the ACK field in the SYNACK segment is 1.

![1-5](week/4/1-5.PNG)

### 6. What is the sequence number of the TCP segment containing the HTTP POST command? Note that in order to find the POST command, you’ll need to dig into the packet content field at the bottom of the Wireshark window, looking for a segment with a “POST” within its DATA field.

The sequence number of the TCP segment containing the HTTP POST command is 4. The sequence number of this segment is 1.

![1-6](week/4/1-6.PNG)

### 7. Consider the TCP segment containing the HTTP POST as the first segment in the TCP connection. What are the sequence numbers of the first six segments in the TCP connection (including the segment containing the HTTP POST)? At what time was each segment sent? When was the ACK for each segment received? Given the difference between when each TCP segment was sent, and when its acknowledgement was received, what is the RTT value for each of the six segments? What is the EstimatedRTT value (see Section 3.5.3, page 242 in text) after the receipt of each ACK? Assume that the value of the EstimatedRTT is equal to the measured RTT for the first segment, and then is computed using the EstimatedRTT equation on page 242 for all subsequent segments.
#### Note: Wireshark has a nice feature that allows you to plot the RTT for each of the TCP segments sent. Select a TCP segment in the “listing of captured packets” window that is being sent from the client to the gaia.cs.umass.edu server. Then select: Statistics->TCP Stream Graph->Round Trip Time Graph.

The segments 1-6 are No. 3, 4, 6, 9, 12, and 14, respectively.  

**Segment 1 sequence number** - `1`  
**Segment 2 sequence number** - `1`  
**Segment 3 sequence number** - `566`  
**Segment 4 sequence number** - `2026`  
**Segment 5 sequence number** - `3486`  
**Segment 6 sequence number** - `4946`  

The RTT (seconds) for all segments are listed below.  

**Segment 1** - 0.000093000  
**Segment 2** - 0.003212000  
**Segment 3** - 0.027460000  
**Segment 4** - 0.035557000  
**Segment 5** - 0.070059000  
**Segment 6** - 0.114428000  

![1-7](week/4/1-7.PNG)

### 8. What is the length of each of the first six TCP segments?

The length of the first TCP segment is 565 bytes; the second is 1460; the third is 1147; and all the rest are 892.

![1-8](week/4/1-8.PNG)

### 9. What is the minimum amount of available buffer space advertised at the received for the entire trace? Does the lack of receiver buffer space ever throttle the sender?

The first ACK from the server shows the minimum amount of available buffer space. In this trace, the value is 17520.

![1-9](week/4/1-9.PNG)

### 10. Are there any retransmitted segments in the trace file? What did you check for (in the trace) in order to answer this question?

Yes, there are many retransmitted segments in the trace file. This is described by the constant line on the time sequence graph (Stevens).

![1-10](week/4/4.PNG)

### 11. How much data does the receiver typically acknowledge in an ACK? Can you identify cases where the receiver is ACKing every other received segment (see Table 3.2 on page 250 in the text).

The receiver is ACKing every other segment and in this example, line no. 7 acknowledged 1514 bytes.

![1-11](week/4/1-11.PNG)

### 12. What is the throughput (bytes transferred per unit time) for the TCP connection? Explain how you calculated this value.

The throughput for the TCP connection is 152,138 bytes / (5.651141000-0.023265000) sec = 27032.93  

The throughput is 27KByte/sec.  

I got this value because throughput is calculated by *file size / (first TCP seg-last ack time)*.  

## 4. TCP congestion control in action

![4](week/4/4.PNG)

### 13. Use the Time-Sequence-Graph(Stevens) plotting tool to view the sequence number versus time plot of segments being sent from the client to the gaia.cs.umass.edu server. Can you identify where TCP’s slowstart phase begins and ends, and where congestion avoidance takes over? Comment on ways in which the measured data differs from the idealized behavior of TCP that we’ve studied in the text.

After using the plotting tool, it was clear that the segments being sent to the client were constant in sequence number over time. This could potentially mean an error since TCP uses 1s complement to indicate errors.

![1-13](week/4/1-13.PNG)
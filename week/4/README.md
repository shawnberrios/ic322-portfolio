# Week 4: Transport Layer: TCP and UDP

This week introduces the transport layer and its major means of transporting data like Kodak Black.

* [Main](wireshark.md)
* [I ate my vegetables](week/4/vegetables.md)
* [Learning Objectives](week/4/learning-objectives.md)
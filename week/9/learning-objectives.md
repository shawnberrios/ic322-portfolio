# Learning Objectives

#### LO1. I can describe how Link State algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Link State algorithm.
Link state algorithms necessitate access to comprehensive global network information to determine the most efficient path from a source to a destination. This requirement involves having knowledge of the connectivity between all network nodes and the associated link costs. A key algorithm used in link-state routing is Dijkstra's algorithm, which is employed to compute the least-cost path from one node to all other nodes. As the algorithm progresses through its iterations, it reveals the least-cost paths to an increasing number of destination nodes at a not so gradual rate. The algorithm maintains certain variables, including:  

D(v): representing the cost of the least-cost path from the source node to destination v.  
p(v): denoting the previous node along the current least-cost path from the source to v.  
N': a subset of nodes, where v is included if the least-cost path from the source to v is known.  

There are multiple examples of Dijkstra's algorithm [here](week/9/vegetables.md)!

Link-state algorithms offer several advantages, such as preventing the creation of loops that can occur with distance-vector algorithms. They also exhibit efficient performance with a time complexity of O(n^2). Additionally, each node independently computes its own forwarding tables, contributing to the robustness of the system, as a malfunctioning node does not lead to erroneous calculations for other nodes. However, link-state algorithms are susceptible to oscillations, wherein the data transmission direction alternates. One potential solution involves mandating that link costs remain independent of the volume of traffic they carry. Another approach is to coordinate the timing of algorithm execution among routers to avoid conflicts. This algorithm does not have the count to infinity problem.

#### LO2. I can describe how Distance Vector algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Distance Vector algorithm.

Distance Vector algorithms compute the least-cost path through an iterative, distributed approach involving routers. This approach is classified as a decentralized routing algorithm, where no single node possesses complete information regarding the costs of all network links. Initially, nodes only possess knowledge about their directly connected links, and they progressively build knowledge about the least-cost paths to neighboring nodes through a series of calculations and information exchanges.

One notable advantage of the Distance Vector (DV) algorithm is its ability to initiate calculations without requiring all data upfront; it accumulates this information during the calculation process. To address issues related to this incremental knowledge gathering, a technique called "poisoned reverse" can be employed. In this solution, if router z routes through router y to reach destination x, router z notifies router y that its distance to x is infinity. This ensures that router y will not attempt to route itself to x via router z while router z routes to x through router y. As soon as a new least-cost path emerges that doesn't involve router y in the route to z, the actual cost is shared, and a new cost of infinity is advertised. It's important to note that this approach resolves the problem for a two-node scenario but doesn't eliminate the possibility of routing loops involving three or more nodes.

To walk through a vector exchange systematically, there is a method one could follow:
1. Each node receives information about its directly connected neighbors, including the cost of reaching those neighbors.
2. Nodes share information about their own neighbors with their connected nodes, allowing nodes to learn the cost from their neighbors to their neighbors' neighbors.
3. Nodes adjust their own costs and connections based on the information received from their neighbors.
4. After updating their information, nodes share their updated data with their connected nodes once again.
5. This process persists until there are no further updates to exchange.
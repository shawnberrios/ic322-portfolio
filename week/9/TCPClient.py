#import socket module
from socket import *
import sys # In order to terminate the program

serverName='localhost'
serverPort=12000
print("Enter file name ->")
fname = input()
clientSocket = socket(AF_INET, SOCK_STREAM)
try:
    clientSocket.connect((serverName, serverPort))
except:
    print("Server is off")
    clientSocket.close()
    sys.exit()
print("Connection OK.")
httpRequest = "GET /" + fname + " HTTP/1.1\r\n\r\n"
clientSocket.send(httpRequest.encode())
print("Request message sent.")

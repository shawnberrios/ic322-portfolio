# Kurose, Chapter 5 Questions

#### R5. What is the "count to infinity" problem in distance vector routing?
The problem describes when a link cost increases, it creates a routing loop. It is possible for in these cases, Dijikstra's algorithm will take too long to converge because the problem is running indefinitely.

#### R8. True or false: When an OSPF route sends its link state information, it is sent only to those nodes directly attached neighbors. Explain.
False. OSPF is a link state routing protocol. This protocol only works by sending each node in the network broadcast link-state packets which contain identities and cost of its attached links. 

#### R9. What is meant by an *area* in an OSPF autonomous system? Why was the concept of an area introduced?
In every autonomous system, one OSPF area is used as the backbone area that is responsible for routing traffic between other areas in the autonomous system. OSPF helps the router know the available link states. The concept of area is used to route packets in traffic.

#### P3. Consider the following network. With the indicated link costs, use Dijkstra’s shortest-path algorithm to compute the shortest path from x to all network nodes. Show how the algorithm works by computing a table similar to Table 5.1.
![x](week/9/photos/x.PNG)

#### P4. Consider the network shown in Problem P3. Using Dijkstra’s algorithm, and showing your work using a table similar to Table 5.1, do the following: (only nodes v, y, and w)
![v](week/9/photos/v.PNG)
![y](week/9/photos/y.PNG)
![w](week/9/photos/w.PNG)

#### P5. Consider the network shown below. Assume that each node initially knows the costs to each of its neighbors. Consider the distance-vector algorithm and show the distance table entries at node z.
![w](week/9/photos/p5.PNG)

#### P11. Consider Figure 5.7. Suppose there is another router w, connected to router y and z. The costs of all links are given as follows: c(x,y) = 4, c(x,z) = 50, c(y,w) = 1, c(z,w) = 1, c(y,z) = 3. Suppose that poisoned reverse is used in the distance-vector routing algorithm.

##### a. When the distance vector routing is stabilized, router w, y, and z inform their distances to x to each other. What distance values do they tell each other?
- y will have a distance of 4 from w and z  
- w will have a distance of 6 to y since x is infinity  
- z will have a distance of 5 to w since w tells y is infinity  

##### b. Now suppose that the link cost between x and y increases to 60. Will there be a count-to-infinity problem even if poisoned reverse is used? Why or why not? If there is a count-to-infinity problem, then how many iterations are needed for the distance-vector routing to reach a stable state again? Justify your answer.
Yes, there will be a count-to-infinity problem. This is because of the loop between the three routers of the previous question. It will take too many iterations for the nodes to eventually be constant. The state count is going back and forth, taking some 11 loops for Dy(x) to be a constant 60.  

##### c. How do you modify c(y,z) such that there is no count-to-infinity problem at all if c(y,x) changes from 4 to 60?
You can remove the link or implace a higher threshold so that the loop is never entered. This means increasing c(y,x).
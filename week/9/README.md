# Week 9: More Network Layer: Control Plane

This week introduces more about the network layer, specifically the control plane!

* [Main](week/9/simple-server.md)
* [Partner Feedback](https://gitlab.com/owenpitch4d/ic322-portfolio/-/issues/13)
* [I ate my vegetables](week/9/vegetables.md)
* [Learning Objectives](week/9/learning-objectives.md)
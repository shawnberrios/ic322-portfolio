# import socket module
from socket import *
import sys # In order to terminate the program
serverSocket = socket(AF_INET, SOCK_STREAM) #Prepare a sever socket
#Fill in start
serverPort=12000
serverSocket=socket(AF_INET,SOCK_STREAM)
serverSocket.bind(('localhost',serverPort))
serverSocket.listen(1)
#Fill in end
while True:
 #Establish the connection
 print('Ready to serve...')
 connectionSocket, addr = serverSocket.accept()
 try:
    message = connectionSocket.recv(1084)
    filename = message.split()[1]
    f = open(filename[1:])
    outputdata = f.read()
    #Send one HTTP header line into socket
    #Fill in start
    hLine="HTTP/1.1 200 OK\r\n"
    connectionSocket.send(hLine.encode())
    connectionSocket.send("\r\n".encode())
    #Fill in end
    #Send the content of the requested file to the client
    for i in range(0, len(outputdata)):
        connectionSocket.send(outputdata[i].encode())
    connectionSocket.send("\r\n".encode())

    connectionSocket.close()
 except IOError:
    #Send response message for file not found
    print("File not found!")
    #Fill in start
    errHead="HTTP/1.1 404 Not Found\r\n"
    connectionSocket.send(errHead.encode())
    connectionSocket.send("\r\n".encode())
    #Fill in end
    #Close client socket
    connectionSocket.close()
    #Fill in start
    #Fill in end

    serverSocket.close()
    sys.exit()  # Terminate the program after sending the corresponding data 

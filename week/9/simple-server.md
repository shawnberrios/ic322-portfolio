# Simple Server Lab

## Introduction
This lab is about establishing a TCP connection through Python that uses HTTP to communicate. Ultimately, this lab is about understanding how web servers process information and the use of GET requests/responses. This specific server reads ".png" as bytes and sends regular data through the server-side with byte-encoded strings.

## Process

The process was fairly simple given that the book gave me skeleton code to structure my TCP client-server connection.

## Code
[Client Code](week/2/TCPClient.py)
[Server Code](week/2/TCPServer.py)

## Questions

## 1. Why are we focusing on the TCP server in this lab rather than the UDP server?
This lab focuses on the TCP server opposed to a UDP one because since it is connection-oriented, a connection needs to be established before any data is sent to either end. TCP allows for a welcoming socket that continues to listen and look for any incoming connections. UDP needs a destination address of each packet in order for the UDP server socket to connect to several clients.

## 2. Look carefully at Figure 2.9 (General format of an HTTP response message). Notice that there's a blank line between the header section and the body. And notice that the blank line is two characters: 'cr' and 'lf'. What are these characters and how do we represent them in our Python response string?
The characters `cr` and `lf` indicate the end of a line. They are represented as `\r` and `\n`, respectively.

## 3. When a client requests the "/index.html" file, where on your computer will your server look for that file?
It looks in the same directory as where the server is.